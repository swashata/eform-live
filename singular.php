<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eForm_Live_Preview
 */

get_header();

// Main wrapper
echo '<main class="efl-page-articles" role="main" id="main">';

if ( have_posts() ) {
	/* Start the Loop */
	while ( have_posts() ) {
		the_post();
		/**
		 * Include the Post-Format-specific template for the content.
		 * If you want to override this in a child theme, then include a file
		 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
		 */
		get_template_part( 'template-parts/content', get_post_format() );
	}
} else {
	get_template_part( 'template-parts/content', 'none' );
}

echo '</main>'; // #main

eform_live_bullhorn();

// Footer
get_footer();
