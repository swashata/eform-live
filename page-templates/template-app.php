<?php
/**
 * Template Name: App Page Template
 *
 * The template for displaying app pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 */

get_header();

// Main wrapper
echo '<main class="efl-page-articles" role="main" id="main">';

if ( have_posts() ) {
	/* Start the Loop */
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', 'page-app' );
	}
} else {
	get_template_part( 'template-parts/content', 'none' );
}

echo '</main>'; // #main

eform_live_bullhorn();

// Footer
get_footer();
