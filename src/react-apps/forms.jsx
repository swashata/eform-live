import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Spinner from './components/Spinner';

const Forms = React.lazy(() =>
	import(/* webpackChunkName: "form-app" */ './components/Forms')
);

const App = props => (
	<React.Suspense fallback={<Spinner />}>
		<Forms {...props} />
	</React.Suspense>
);

function mountForms(Node) {
	const data = Node.dataset.forms;
	try {
		const props = JSON.parse(data);
		ReactDOM.render(<App data={props} />, Node);
	} catch (e) {
		console.error('Invalid Props');
	}
}

document.addEventListener('DOMContentLoaded', () => {
	const formNodes = document.querySelectorAll('.efl-live-form-examples');
	formNodes.forEach(n => mountForms(n));
});
