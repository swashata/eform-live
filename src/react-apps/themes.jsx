import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Spinner from './components/Spinner';

const Themes = React.lazy(() =>
	import(/* webpackChunkName: "theme-app" */ './components/Themes')
);

const App = props => (
	<React.Suspense fallback={<Spinner />}>
		<Themes {...props} />
	</React.Suspense>
);

function mountThemes(Node) {
	const data = Node.dataset.themes;
	try {
		const props = JSON.parse(data);
		ReactDOM.render(<App data={props} />, Node);
	} catch (e) {
		console.error('Invalid Props');
	}
}

document.addEventListener('DOMContentLoaded', () => {
	const formNodes = document.querySelectorAll('.efl-live-form-themes');
	formNodes.forEach(n => mountThemes(n));
});
