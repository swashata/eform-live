export function addQueryArgs(uri, param) {
	let url = uri;
	let separator = url.indexOf('?') !== -1 ? '&' : '?';
	Object.keys(param).forEach(key => {
		if (param[key] === null) {
			return;
		}
		url = `${url}${separator}${key}=${param[key]}`;
		separator = '&';
	});
	return url;
}
