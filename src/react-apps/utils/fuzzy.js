/**
 * FuzzySearch Implementation
 *
 * Adopted from
 * {@link https://github.com/bevacqua/fuzzysearch}
 *
 * Changed the implementation a little bit to compare
 * against lowercase values and support unicode.
 *
 * The MIT License (MIT)
 * Copyright © 2015 Nicolas Bevacqua
 * @param {string} needle
 * @param {string} haystack
 */
/* eslint-disable */
/* istanbul ignore next */
export function fuzzySearch(needle, haystack) {
	needle = needle.toLowerCase();
	haystack = haystack.toLowerCase();
	let hlen = haystack.length;
	let nlen = needle.length;
	if (nlen > hlen) {
		return false;
	}
	if (nlen === hlen) {
		return needle === haystack;
	}
	outer: for (let i = 0, j = 0; i < nlen; i++) {
		let nch = needle.codePointAt(i);
		while (j < hlen) {
			if (haystack.codePointAt(j++) === nch) {
				continue outer;
			}
		}
		return false;
	}
	return true;
}
/* eslint-enable */
