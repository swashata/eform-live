import * as React from 'react';
import styled from 'styled-components';
import { Spring, animated, config } from 'react-spring';

import IFrame from '../IFrame';

import SafariNewIcon from '../icons/safarinew.svg';
import DesktopIcon from '../icons/desktop.svg';
import TabletIcon from '../icons/tablet.svg';
import MobileIcon from '../icons/mobile.svg';
import RefreshIcon from '../icons/refresh.svg';
import ArrowRightIcon from '../icons/arrow-right.svg';
import ArrowLeftIcon from '../icons/arrow-left.svg';
import SidebarIcon from '../icons/sidebar.svg';

const SafariOuter = styled.div`
	background: #fff;
	border-radius: 5px;
	box-shadow: 0 15px 47px rgba(0, 0, 0, 0.45);
	overflow: hidden;
`;

const Panel = styled.header`
	background-image: linear-gradient(to bottom, #ecebec 0%, #d6d5d6 100%);
	height: 37px;
	box-shadow: 0 1px 0 #b6b5b6, inset 0 1px 0 rgba(255, 255, 255, 0.5);
	display: flex;
	align-items: center;
	position: relative;
	padding-right: 24px;
`;

const Control = styled.aside`
	height: 12px;
	display: flex;
	padding: 0 14px;

	span {
		height: 12px;
		width: 12px;
		border-radius: 100%;
		border: 1px solid;
		margin: 0 10px 0 0;

		&:nth-child(1) {
			background-color: #fa6058;
			border-color: #e14540;
		}
		&:nth-child(2) {
			background-color: #fcbd2e;
			border-color: #e0a027;
		}
		&:nth-child(3) {
			background-color: #30ca41;
			border-color: #2ead31;
			margin-right: 0;
		}
	}
`;

const TitleWrap = styled.div`
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const Title = styled.h3`
	height: 22px;
	border-radius: 4px;
	background: #fff;
	box-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);
	padding: 0 20px;
	text-transform: lowercase;
	font-size: 12px;
	line-height: 22px;
	margin: 0 auto;
	width: 80%;
	text-align: center;
	position: relative;
	color: #000;
	user-select: none;
`;

const TitleIcon = styled.div`
	position: absolute;
	right: 2px;
	top: 1px;
	height: 22px;
	width: 22px;
	font-size: 15px;
	color: #7e7e7e;
`;

const Actions = styled.aside`
	display: flex;
	margin-right: 10px;
`;

const ActionButton = styled.a`
	width: 37px;
	height: 22px;
	margin-right: 8px;
	background: #fcfcfc;
	border-radius: 4px;
	display: flex;
	box-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);
	font-size: 14px;
	color: ${props =>
		// eslint-disable-next-line no-nested-ternary
		props.active ? '#000' : props.disabled ? '#bbb' : '#666'};
	align-items: center;
	justify-content: center;
	transition: background 200ms ease-out, color 200ms ease-out;

	&:hover {
		background: ${props => (props.disabled ? '#fcfcfc' : '#f2f2f2')};
		cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
		color: ${props => (props.disabled ? '#bbb' : '#000')};
	}

	&:active {
		box-shadow: ${props =>
			props.disabled
				? '0 1px 0 rgba(0, 0, 0, 0.1)'
				: 'inset 0 1px 0 rgba(0, 0, 0, 0.1)'};
	}

	&:last-of-type {
		margin-right: 0;
	}
`;

const ActionButtonSmall = styled(ActionButton)`
	width: 26px;
	margin-right: 1px;
	font-size: 12px;
`;

const NewIcon = styled.div`
	position: absolute;
	right: 0;
	bottom: 0;
	font-size: 11px;
	height: 24px;
	width: 24px;
	background-image: linear-gradient(to bottom, #d5d4d5 0%, #c7c6c7 100%);
	box-shadow: inset 1px 1px 0 #adadad;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const BodyWrap = styled.main`
	padding: 1px;
	width: 100%;
	overflow-x: auto;
`;

const Body = animated(
	styled.div`
		height: ${props => props.height};
		position: relative;
		margin: 0 auto;
		width: 100%;
	`
);

export default class Safari extends React.Component {
	static getDerivedStateFromProps(props, state) {
		if (state.mode === 'desktop' && !props.desktop) {
			return { mode: 'tablet' };
		}
		return null;
	}

	state = {
		mode: 'desktop',
	};

	openLink = e => {
		e.preventDefault();
		const { url } = this.props;
		const win = window.open(url, 'el-safari-tab');
		win.focus();
	};

	render() {
		const {
			title,
			minimal = false,
			desktop = false,
			url,
			height = '700px',
			nextAction,
			prevAction,
		} = this.props;
		const { mode } = this.state;
		let width = '1200px';
		if (mode === 'mobile') {
			width = '480px';
		} else if (mode === 'tablet') {
			width = '768px';
		}

		return (
			<SafariOuter>
				<Panel>
					{!minimal && (
						<Control>
							<span />
							<span />
							<span />
						</Control>
					)}
					{!minimal && (
						<Actions>
							<ActionButtonSmall
								title="previous color"
								disabled={!prevAction}
								onClick={e => {
									e.preventDefault();
									if (prevAction) {
										prevAction();
									}
								}}
							>
								{/* go to left color */}
								<ArrowLeftIcon />
							</ActionButtonSmall>
							<ActionButtonSmall
								title="next color"
								disabled={!nextAction}
								onClick={e => {
									e.preventDefault();
									if (nextAction) {
										nextAction();
									}
								}}
							>
								{/* go to right color */}
								<ArrowRightIcon />
							</ActionButtonSmall>
						</Actions>
					)}
					{!minimal && (
						<Actions style={{ marginRight: 0 }}>
							{/* open form in new window */}
							<ActionButton
								style={{ fontSize: '18px' }}
								title="open in new window"
								href="#"
								onClick={this.openLink}
							>
								<SidebarIcon />
							</ActionButton>
						</Actions>
					)}
					<TitleWrap>
						<Title>
							<span>{title}</span>
							<TitleIcon>
								<RefreshIcon />
							</TitleIcon>
						</Title>
					</TitleWrap>
					{!minimal && (
						<Actions>
							<ActionButton
								active={mode === 'mobile'}
								onClick={e => {
									e.preventDefault();
									this.setState({ mode: 'mobile' });
								}}
								href="#"
								title="mobile view"
							>
								<MobileIcon />
							</ActionButton>
							<ActionButton
								active={mode === 'tablet'}
								onClick={e => {
									e.preventDefault();
									this.setState({ mode: 'tablet' });
								}}
								href="#"
								title="tablet view"
							>
								<TabletIcon />
							</ActionButton>
							{desktop ? (
								<ActionButton
									active={mode === 'desktop'}
									onClick={e => {
										e.preventDefault();
										this.setState({ mode: 'desktop' });
									}}
									href="#"
									title="desktop view"
								>
									<DesktopIcon />
								</ActionButton>
							) : null}
						</Actions>
					)}
					<NewIcon>
						<SafariNewIcon />
					</NewIcon>
				</Panel>
				<Spring
					from={{ maxWidth: '100px' }}
					to={{ maxWidth: width }}
					config={config.gentle}
				>
					{props => (
						<BodyWrap>
							<Body style={props} height={height}>
								<IFrame title={title} url={url} />
							</Body>
						</BodyWrap>
					)}
				</Spring>
			</SafariOuter>
		);
	}
}
