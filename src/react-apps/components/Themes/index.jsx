import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import styled from 'styled-components';

import AppTheme from '../AppTheme';
import NeedCollapse from '../NeedCollapse';
import Sidebar, { SidebarLayout, Widget } from '../Sidebar';
import Filter from '../Filter';
import Colors from '../Colors';
import Safari from '../Safari';
import { addQueryArgs } from '../../utils/queryArgs';

const ThemeWindow = styled.section`
	margin-bottom: 40px;
	@media screen and (min-width: ${props => props.theme.breakpointTablet}) {
		margin-bottom: 80px;
	}
`;

class Theme extends React.Component {
	constructor(...args) {
		super(...args);
		const {
			data: { colors, styles, variants },
		} = this.props;
		this.state = {
			color: colors[0].id,
			style: styles[0].id,
			variant: variants[0].id,
		};
	}

	setColor = id => this.setState({ color: id });

	setStyle = id => this.setState({ style: id });

	setVariant = id => this.setState({ variant: id });

	getActiveColor = () => {
		const { color } = this.state;
		const {
			data: { colors },
		} = this.props;
		const activeColorIndex = colors.findIndex(c => c.id === color);
		return [activeColorIndex, colors[activeColorIndex]];
	};

	render() {
		const { color, style, variant } = this.state;
		const {
			data: { colors, styles, variants, url },
		} = this.props;

		const [activeColorIndex, activeColor] = this.getActiveColor();

		const prevAction =
			activeColorIndex === 0
				? null
				: () => this.setColor(colors[activeColorIndex - 1].id);
		const nextAction =
			activeColorIndex === colors.length - 1
				? null
				: () => this.setColor(colors[activeColorIndex + 1].id);

		const qParams = {
			element_style: style,
			fsqm_theme:
				variant === 'light'
					? `material-${color}`
					: `material-d-${color}`,
		};

		const formUrl = addQueryArgs(url.url, qParams);
		return (
			<AppTheme>
				<>
					<NeedCollapse threshold={[768, 1024, 1610]}>
						{([
							needCollapseTablet,
							needCollapseDesktop,
							needCollapseHd,
						]) => (
							<SidebarLayout
								className="efl-ra-themes"
								needCollapse={needCollapseDesktop}
							>
								<Sidebar
									className="efl-ra-themes__sidebar"
									needCollapse={needCollapseDesktop}
								>
									<Widget title="Style">
										<Filter
											items={styles}
											active={style}
											onClick={this.setStyle}
										/>
									</Widget>
									<Widget title="Variant">
										<Filter
											items={variants}
											active={variant}
											onClick={this.setVariant}
										/>
									</Widget>
									<Widget title="Color">
										<Colors
											items={colors}
											active={color}
											onClick={this.setColor}
										/>
									</Widget>
								</Sidebar>
								<ThemeWindow className="efl-ra-themes__window">
									<Safari
										minimal={needCollapseTablet}
										desktop={!needCollapseHd}
										title={activeColor.name}
										url={formUrl}
										height={
											// eslint-disable-next-line no-nested-ternary
											needCollapseTablet
												? '400px'
												: needCollapseDesktop
												? '600px'
												: '650px'
										}
										prevAction={prevAction}
										nextAction={nextAction}
									/>
								</ThemeWindow>
							</SidebarLayout>
						)}
					</NeedCollapse>
				</>
			</AppTheme>
		);
	}
}

export default hot(Theme);
