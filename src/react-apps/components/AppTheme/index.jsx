import * as React from 'react';
import { ThemeProvider } from 'styled-components';

import { color } from '../../utils/colors';

export default function AppTheme({ children }) {
	return <ThemeProvider theme={color}>{children}</ThemeProvider>;
}
