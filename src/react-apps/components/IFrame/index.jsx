import * as React from 'react';
import styled from 'styled-components';

import Spinner from '../Spinner';

const IFrameWrap = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	-webkit-overflow-scrolling: touch;
	overflow-y: auto;

	iframe {
		height: 100%;
		width: 100%;

		@media screen and (min-width: 1024px) {
			display: block;
			-webkit-overflow-scrolling: touch;
			overflow-y: auto;
		}
	}

	.efl-ra-iframe__spinner {
		background: ${props => props.theme.background};
		z-index: 100;
		display: flex;
		align-items: center;
		justify-content: center;
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
	}
`;

export default class IFrame extends React.Component {
	static getDerivedStateFromProps(props, state) {
		if (props.url !== state.url) {
			return { loaded: false, url: props.url };
		}

		return null;
	}

	state = {
		loaded: false,
		url: null,
	};

	handleLoad = () => this.setState({ loaded: true });

	render() {
		const { url, loaded } = this.state;
		const { title } = this.props;
		return (
			<IFrameWrap className="efl-ra-iframe">
				<iframe
					className="efl-ra-iframe__frame"
					title={title}
					src={url}
					frameBorder="0"
					onLoad={this.handleLoad}
				/>
				{!loaded ? (
					<div className="efl-ra-iframe__spinner">
						<Spinner />
					</div>
				) : null}
			</IFrameWrap>
		);
	}
}
