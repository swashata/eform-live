import * as React from 'react';
import './index.scss';

export default function Spinner() {
	return (
		<div className="efl-dl">
			<div className="efl-dl__wrap">
				<div className="efl-dl__container">
					<div className="efl-dl__corner efl-dl__corner--top" />
					<div className="efl-dl__corner efl-dl__corner--bottom" />
				</div>
				<div className="efl-dl__square" />
			</div>
		</div>
	);
}
