import * as React from 'react';
import styled from 'styled-components';

const ColorGrid = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr 1fr 1fr;
	@media screen and (min-width: ${props => props.theme.breakpointDesktop}) {
		grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
	}
	grid-gap: 10px;
`;

const ColorGridItem = styled.a`
	display: block;
	position: relative;
	background-color: ${props => props.color};
	border-radius: 2px;
	transition: box-shadow 200ms ease-out, transform 200ms ease-out;
	border: 2px solid ${props => props.color};

	/* hack to make it square, no matter the width */
	&::before {
		content: '';
		display: block;
		position: relative;
		padding-top: 100%;
	}

	/* for active state */
	/* border-color: ${props => (props.active ? '#fff' : props.color)}; */
	box-shadow: ${props =>
		props.active ? '0 2px 6px rgba(0, 0, 0, .32)' : 'none'};
	transform: ${props => (props.active ? 'scale(1.25)' : 'scale(1)')};

	&:hover {
		transform: scale(1.15);
	}
`;

export default function Colors({ items, active, onClick }) {
	return (
		<ColorGrid>
			{items.map(item => (
				<ColorGridItem
					key={item.id}
					active={active === item.id}
					onClick={e => {
						e.preventDefault();
						onClick(item.id);
					}}
					color={item.code}
					title={item.name}
					href="#"
				/>
			))}
		</ColorGrid>
	);
}
