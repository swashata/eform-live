import * as React from 'react';
import { Spring, config } from 'react-spring';

export default class HoverSpin extends React.Component {
	state = {
		hover: false,
	};

	handleHvrMouseIn = () => this.setState({ hover: true });

	handleHvrMouseOut = () => this.setState({ hover: false });

	render() {
		const { children, native = true, rotate = 180 } = this.props;
		const { hover } = this.state;
		return (
			<Spring
				native={native}
				from={{ transform: 'rotate(0deg)' }}
				to={{
					transform: hover ? `rotate(${rotate}deg)` : 'rotate(0deg)',
				}}
				config={config.wobbly}
			>
				{style =>
					children({
						style,
						onMouseEnter: this.handleHvrMouseIn,
						onMouseLeave: this.handleHvrMouseOut,
					})
				}
			</Spring>
		);
	}
}
