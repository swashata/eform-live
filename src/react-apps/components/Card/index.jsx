import * as React from 'react';
import styled from 'styled-components';
import { animated } from 'react-spring';

const Title = styled.h2`
	font-weight: bold;
	font-size: 20px;
	margin: 0 0 10px 0;
	padding: 0 10px;
	color: ${props => props.theme.text};
`;

const Subtitle = styled.h3`
	font-weight: normal;
	font-size: 14px;
	color: ${props => props.theme.textLight};
	margin: auto 0 20px 0;
	padding: 0 10px;
`;

const CardWrap = animated(
	styled.div`
		display: flex;
		flex-flow: column nowrap;
		padding: 15px 10px;
		border-radius: 4px;
		background: ${props => props.theme.background};
		box-shadow: 0 4px 10px rgba(0, 0, 0, 0.12);
	`
);

const ActionGroup = styled.div`
	display: flex;
	flex-flow: row wrap;
`;

export const CardAction = styled.a`
	height: 28px;
	padding: 0 10px;
	text-transform: uppercase;
	line-height: 28px;
	flex: 0 0 auto;
	border-radius: 4px;
	font-size: 14px;
	transition: background-color 200ms ease-out, color 200ms ease-out;
	color: ${props => props.theme.primary};
	&:hover {
		background-color: ${props => props.theme.primary};
		color: ${props => props.theme.primaryText};
	}
	&:active,
	&:focus {
		box-shadow: 0 0 0 1px ${props => props.theme.primaryFocus};
		outline: none;
	}
`;

export default function Card({ title, subtitle, actions, style = {} }) {
	return (
		<CardWrap className="efl-ra-cards" style={style}>
			<Title className="efl-ra-cards__title">{title}</Title>
			<Subtitle className="efl-ra-cards__subtitle">{subtitle}</Subtitle>
			{actions ? (
				<ActionGroup className="efl-ra-cards__actions">
					{actions}
				</ActionGroup>
			) : null}
		</CardWrap>
	);
}
