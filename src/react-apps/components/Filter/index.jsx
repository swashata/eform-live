import * as React from 'react';
import styled from 'styled-components';

import TimesIcon from '../icons/times.svg';

const FilterItem = styled.a`
	background: ${props =>
		props.active ? props.theme.whiteShadeOne : props.theme.background};
	display: flex;
	padding: 6px 12px;
	margin: 3px 0;
	align-items: center;
	justify-content: flex-start;
	flex-flow: row nowrap;
	color: ${props =>
		props.active ? props.theme.text : props.theme.textLight};
	border-radius: 4px;
	transition: background-color 200ms ease-out, color 200ms ease-out;

	&:hover {
		background: ${props => props.theme.whiteShadeTwo};
	}

	.efl-filter__bullet {
		height: 8px;
		width: 8px;
		margin: 0 6px;
		border-radius: 100%;
		background-color: ${props =>
			props.active ? props.theme.success : props.theme.textLight};
		transition: background-color 200ms ease-out;
	}
`;

const FilterItemWrap = styled.div`
	position: relative;
`;

const FilterItemReset = styled.a`
	position: absolute;
	right: 12px;
	height: 18px;
	width: 18px;
	top: 50%;
	transform: translateY(-50%);
	background: ${props => props.theme.whiteShadeThree};
	display: flex;
	font-size: 14px;
	align-items: center;
	justify-content: center;
	border-radius: 100%;
	color: ${props => props.theme.text};
`;

const Filter = ({ items, onClick, active, reset }) => (
	<div className="efl-filter">
		{items.map(item => (
			<FilterItemWrap key={item.id}>
				<FilterItem
					className="efl-filter__item"
					active={item.id === active}
					href="#"
					onClick={e => {
						e.preventDefault();
						onClick(item.id);
					}}
				>
					<span className="efl-filter__bullet" />
					<span className="efl-filter__label">{item.name}</span>
				</FilterItem>
				{item.id === active && reset ? (
					<FilterItemReset onClick={reset}>
						<TimesIcon />
					</FilterItemReset>
				) : null}
			</FilterItemWrap>
		))}
	</div>
);

export default Filter;
