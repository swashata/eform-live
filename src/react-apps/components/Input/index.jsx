import * as React from 'react';
import styled from 'styled-components';

import { color } from '../../utils/colors';

const Inputelm = styled.input`
	border: 0 none;
	/* height: 36px; */
	width: 100%;
	background-color: ${props => props.theme.whiteShadeOne};
	line-height: 20px;
	color: ${props => props.theme.text};
	font-size: 16px;
	border-radius: 4px;
	padding: 8px 10px;
	padding-left: ${props => (props.hasIcon ? '32px' : '10px')};
	transition: background-color 200ms ease-out, box-shadow 200ms ease-out;
	box-shadow: 0 0 0 transparent;

	&::placeholder {
		color: ${props => props.theme.textLight};
	}

	&:hover {
		background-color: ${props => props.theme.whiteShadeTwo};
	}
	&:active,
	&:focus {
		background-color: ${props => props.theme.background};
		box-shadow: 0 1px 4px rgba(0, 0, 0, 0.12);
		outline: none;
	}

	@media screen and (min-width: ${props => props.theme.breakpointDesktop}) {
		/* height: 42px; */
		line-height: 26px;
		padding-left: ${props => (props.hasIcon ? '44px' : '10px')};
	}
`;

Inputelm.defaultProps = {
	theme: { ...color },
};

const InputWrap = styled.div`
	position: relative;
`;

const InputIcon = styled.div`
	position: absolute;
	left: 10px;
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	color: ${props => props.theme.textLight};
	width: 16px;
	font-size: 16px;
	@media screen and (min-width: ${props => props.theme.breakpointDesktop}) {
		font-size: 18px;
		left: 15px;
		width: 18px;
	}
`;

export default function Input({ icon, ...props }) {
	const hasIcon = icon != null;
	return (
		<InputWrap>
			{hasIcon ? <InputIcon>{icon}</InputIcon> : null}
			<Inputelm hasIcon={hasIcon} {...props} />
		</InputWrap>
	);
}
