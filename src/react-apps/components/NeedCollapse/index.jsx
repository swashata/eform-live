import * as React from 'react';
import { throttle } from 'throttle-debounce';

export default class NeedCollapse extends React.Component {
	static defaultProps = {
		threshold: 1024,
	};

	handleWindowResize = throttle(300, () => {
		const { needCollapse } = this.state;
		const needCollapseNow = this.sidebarNeedCollapse();
		if (needCollapse !== needCollapseNow) {
			this.setState({ needCollapse: needCollapseNow });
		}
	});

	state = {
		needCollapse: this.sidebarNeedCollapse(),
	};

	componentDidMount() {
		window.addEventListener('resize', this.handleWindowResize);
		this.handleWindowResize();
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.handleWindowResize);
	}

	sidebarNeedCollapse() {
		const width = window.innerWidth;
		const { threshold } = this.props;
		if (Array.isArray(threshold)) {
			const needCollapse = [];
			threshold.forEach(t => {
				if (width < t) {
					needCollapse.push(true);
				} else {
					needCollapse.push(false);
				}
			});
			return needCollapse;
		}
		if (width < threshold) {
			return true;
		}
		return false;
	}

	render() {
		const { children } = this.props;
		const { needCollapse } = this.state;
		return children(needCollapse);
	}
}
