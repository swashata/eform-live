/* eslint-disable react/no-multi-comp */
import * as React from 'react';
import styled from 'styled-components';
import { Transition, Spring, animated } from 'react-spring';
import classNames from 'classnames';
import Swipeable from 'react-swipeable';

import IconBack from '../icons/back.svg';
import IconChecked from '../icons/checked.svg';
import IconSettings from '../icons/settings.svg';

const SidebarWrap = styled.div`
	width: 100%;
	padding: 10px;
`;

const SidebarFixedWrap = animated(
	styled.div`
		width: 238px;
		height: calc(100vh - 50px);
		overflow-y: auto;
		box-shadow: 4px 0 10px rgba(0, 0, 0, 0.18);
		background: ${props => props.theme.background};
		position: fixed;
		z-index: 9998;
		top: 50px;
	`
);

const SidebarStickyWrap = styled.div`
	position: sticky;
	top: 80px;
`;

const SidebarControl = styled.a`
	background-color: ${props => props.theme.background};
	position: fixed;
	display: block;
	bottom: 10px;
	right: 10px;
	height: 62px;
	width: 62px;
	border-radius: 100%;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.32);
	z-index: 99998;
	@media screen and (min-width: ${props => props.theme.breakpointTablet}) {
		height: 72px;
		width: 72px;
		right: 20px;
		bottom: 20px;
	}
`;

const SidebarControlIconWrap = animated(
	styled.span`
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		font-size: 26px;
	`
);

const SidebarWidget = styled.div`
	margin: 0 0 20px 0;
	&:last-of-type {
		margin-bottom: 0;
	}
`;

const SidebarTitle = styled.h3`
	color: ${props => props.theme.textLight};
	text-transform: uppercase;
	font-size: 14px;
	margin: 0 0 10px 0;
	line-height: 14px;
`;

export default class Sidebar extends React.Component {
	state = {
		isOpen: false,
		hasChanged: false,
	};

	sidebarRef = React.createRef();

	sidebarControlRef = React.createRef();

	componentDidMount() {
		['change', 'input', 'click'].forEach(evt => {
			this.sidebarRef.current.addEventListener(evt, this.setHasChanged);
		});
		document.addEventListener('click', this.handleOuterClick);
		document.addEventListener('touchend', this.handleOuterClick);
	}

	componentWillUnmount() {
		// remove ref event listener
		['change', 'input', 'click'].forEach(evt => {
			this.sidebarRef.current.removeEventListener(
				evt,
				this.setHasChanged
			);
		});
		document.removeEventListener('click', this.handleOuterClick);
		document.removeEventListener('touchend', this.handleOuterClick);
	}

	setHasChanged = () => {
		this.setState(({ hasChanged }) =>
			hasChanged ? null : { hasChanged: true }
		);
	};

	handleSidebarToggle = e => {
		e.preventDefault();
		// Reset the hasChanged while toggling
		this.setState(({ isOpen }) => ({ isOpen: !isOpen, hasChanged: false }));
	};

	handleOuterClick = e => {
		const { current: sidebar } = this.sidebarRef;
		const { current: control } = this.sidebarControlRef;
		if (!e.target) {
			return;
		}
		if (sidebar.contains(e.target) || control.contains(e.target)) {
			return;
		}
		if (e.target && e.target.parentElement) {
			const { parentElement } = e.target;
			if (
				sidebar.contains(parentElement) ||
				control.contains(parentElement)
			) {
				return;
			}
		}
		this.closeSidebar();
	};

	closeSidebar = () => {
		const { needCollapse } = this.props;
		const { isOpen } = this.state;
		if (!needCollapse || !isOpen) {
			return;
		}
		this.setState({ isOpen: false, hasChanged: false });
	};

	renderCollapsibleSidebar() {
		const { children } = this.props;
		const { isOpen } = this.state;
		const initialSpring = { left: -238, opacity: 0 };

		return (
			<Spring
				native
				from={{ ...initialSpring }}
				to={isOpen ? { left: 0, opacity: 1 } : { ...initialSpring }}
			>
				{props => (
					<SidebarFixedWrap style={props}>
						<SidebarWrap>{children}</SidebarWrap>
					</SidebarFixedWrap>
				)}
			</Spring>
		);
	}

	renderRegularSidebar() {
		const { children } = this.props;
		return (
			<SidebarStickyWrap>
				<SidebarWrap>{children}</SidebarWrap>
			</SidebarStickyWrap>
		);
	}

	render() {
		const { hasChanged, isOpen } = this.state;
		const { needCollapse, className = null } = this.props;
		let SVGIcon = IconSettings;
		if (isOpen) {
			if (hasChanged) {
				SVGIcon = IconChecked;
			} else {
				SVGIcon = IconBack;
			}
		}
		return (
			<>
				<div
					className={classNames('efl-sidebar', className)}
					ref={this.sidebarRef}
				>
					{needCollapse
						? this.renderCollapsibleSidebar()
						: this.renderRegularSidebar()}
				</div>
				{needCollapse ? (
					<SidebarControl
						ref={this.sidebarControlRef}
						onClick={this.handleSidebarToggle}
					>
						<Transition
							native
							items={SVGIcon}
							// keys={item => item.key}
							from={{
								opacity: 0,
								transform: 'scale(0.4) rotate(-180deg)',
							}}
							enter={{
								opacity: 1,
								transform: 'scale(1) rotate(0deg)',
							}}
							leave={{
								opacity: 0,
								transform: 'scale(0.4) rotate(180deg)',
							}}
						>
							{Icon => props => (
								<SidebarControlIconWrap style={props}>
									<Icon />
								</SidebarControlIconWrap>
							)}
						</Transition>
					</SidebarControl>
				) : null}
			</>
		);
	}
}

export function Widget({ title = null, children }) {
	return (
		<SidebarWidget>
			{title && <SidebarTitle>{title}</SidebarTitle>}
			{children}
		</SidebarWidget>
	);
}

export const SidebarLayout = styled.div`
	display: ${props => (props.needCollapse ? 'block' : 'grid')};
	grid-gap: 40px;
	grid-template-columns: 280px auto;
`;
