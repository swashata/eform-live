/* eslint-disable react/destructuring-assignment */
import * as React from 'react';
import styled from 'styled-components';
import { Transition, config } from 'react-spring';
import { hot } from 'react-hot-loader/root';

import AppTheme from '../AppTheme';
import NeedCollapse from '../NeedCollapse';
import { fuzzySearch } from '../../utils/fuzzy';
import Sidebar, { Widget, SidebarLayout } from '../Sidebar';
import Input from '../Input';
import Filter from '../Filter';
import SearchIcon from '../icons/search.svg';
import Card, { CardAction } from '../Card';
import Modal from '../Modal';

const CardWrap = styled.div`
	display: grid;
	grid-gap: 10px;
	grid-template-columns: 1fr;
	position: relative;

	@media screen and (min-width: ${props => props.theme.breakpointTablet}) {
		grid-template-columns: 1fr 1fr;
		grid-gap: 20px;
	}
	@media screen and (min-width: ${props => props.theme.breakpointHd}) {
		grid-template-columns: 1fr 1fr 1fr;
	}
`;

function getCategoryName(id, cats) {
	const cat = cats.find(c => c.id === id);
	if (cat) {
		return cat.name;
	}
	return 'Unknown';
}

class Forms extends React.Component {
	state = {
		activeCat: null,
		modalOpen: false,
		currentModalForm: null,
		prevModalFormId: null,
		nextModalFormId: null,
		search: '',
	};

	componentDidMount() {
		// popstate is called when user actually presses the back/forward
		// button, not just through the history.pushState
		// https://developer.mozilla.org/en-US/docs/Web/Events/popstate
		window.addEventListener('popstate', this.handlePopstate);

		// Also account for initial state from window hash
		this.initFromHash();
	}

	componentWillUnmount() {
		window.removeEventListener('popstate', this.handlePopstate);
	}

	initFromHash = () => {
		const { hash } = window.location;
		// If it matches category
		const catMatch = /^#efl-ex-cat-(\d+)$/.exec(hash);
		if (catMatch) {
			this.setActiveCat(catMatch[1], false);
			return;
		}
		const formMatch = /^#efl-ex-form-(\d+)$/.exec(hash);
		if (formMatch) {
			this.openModal(formMatch[1], false);
		}
	};

	handlePopstate = e => {
		// If this is from our own app, then respond
		if (e.state && e.state.source === 'efl') {
			const { action, id, type } = e.state;
			// Now if it is a set
			if (action === 'set') {
				switch (type) {
					case 'cat':
						this.setActiveCat(id, false);
						break;
					case 'form':
						this.openModal(id, false);
						break;
					default:
					// nothing
				}
			} else if (action === 'reset') {
				switch (type) {
					case 'cat':
						this.resetActiveCat(false);
						break;
					case 'form':
						this.closeModal(false);
						break;
					default:
					// nothing
				}
			}
		} else if (e.state === null && window.location.hash === '') {
			// Clean up if needed
			if (this.state.activeCat !== null) {
				this.resetActiveCat(false);
			} else if (this.state.modalOpen) {
				this.closeModal(false);
			}
		} else if (e.state === null) {
			// Check if some match can be found from the hash
			this.initFromHash();
		}
	};

	setHistoryHashtag = (d, history = true) => {
		if (!history) {
			return;
		}
		const data = { ...d, action: 'set', source: 'efl' };
		if (window.history) {
			const hash = `#efl-ex-${data.type}-${data.id}`;
			window.history.pushState(data, document.title, hash);
		}
	};

	resetHistoryHashtag = (d, history = true) => {
		if (!history) {
			return;
		}
		const data = { ...d, action: 'reset', source: 'efl' };
		if (window.history) {
			const { pathname, search } = window.location;
			window.history.pushState(
				data,
				document.title,
				`${pathname}${search}`
			);
		}
	};

	setActiveCat = (id, history = true) => {
		// Add history hashtag
		this.setHistoryHashtag({ type: 'cat', id }, history);

		// Set state
		this.setState({ activeCat: id });
	};

	resetActiveCat = (history = true) => {
		// Remove history hashtag
		this.resetHistoryHashtag({ type: 'cat' }, history);

		// Set state
		this.setState({ activeCat: null });
	};

	openModal = (id, history = true) => {
		const forms = this.getActiveForms();
		const formIndex = forms.findIndex(f => f.id === id);
		if (formIndex === -1) {
			return;
		}
		const prevModalFormId = forms[formIndex - 1]
			? forms[formIndex - 1].id
			: null;
		const nextModalFormId = forms[formIndex + 1]
			? forms[formIndex + 1].id
			: null;
		const form = forms[formIndex];
		// Set window history
		this.setHistoryHashtag({ type: 'form', id }, history);
		this.setState({
			modalOpen: true,
			currentModalForm: form,
			prevModalFormId,
			nextModalFormId,
		});
	};

	closeModal = (history = true) => {
		// Reset history hashtag
		this.resetHistoryHashtag({ type: 'form' }, history);
		// Set state
		this.setState({
			modalOpen: false,
		});
	};

	handleSearch = e => this.setState({ search: e.target.value });

	/**
	 * Get active forms by category.
	 */
	getActiveForms = () => {
		const { activeCat } = this.state;
		let filteredForms;
		const {
			data: { forms },
		} = this.props;
		if (!activeCat) {
			filteredForms = forms;
		} else {
			filteredForms = forms.filter(form => form.category === activeCat);
		}
		const { search } = this.state;
		if (search !== '') {
			filteredForms = filteredForms.filter(form =>
				fuzzySearch(search, form.name)
			);
		}

		return filteredForms;
	};

	render() {
		const {
			// eslint-disable-next-line camelcase
			data: { cats, download_url },
		} = this.props;
		const {
			activeCat,
			modalOpen,
			currentModalForm,
			nextModalFormId,
			prevModalFormId,
			search,
		} = this.state;
		const forms = this.getActiveForms();
		return (
			<AppTheme>
				<NeedCollapse>
					{needCollapse => (
						<>
							<SidebarLayout
								className="efl-ra-forms"
								needCollapse={needCollapse}
							>
								<Sidebar
									needCollapse={needCollapse}
									className="efl-ra-forms__sidebar"
								>
									<Widget>
										<Input
											icon={<SearchIcon />}
											type="text"
											placeholder="search examples..."
											value={search}
											onChange={this.handleSearch}
										/>
									</Widget>
									<Widget title="Filter by">
										<Filter
											items={cats}
											active={activeCat}
											onClick={this.setActiveCat}
											reset={this.resetActiveCat}
										/>
									</Widget>
								</Sidebar>
								<CardWrap className="efl-ra-forms__cards">
									<Transition
										items={forms}
										keys={item => item.id}
										from={{
											transform: 'translate3d(0,-20px,0)',
											opacity: 0,
											position: 'static',
										}}
										enter={{
											transform: 'translate3d(0,0px,0)',
											opacity: 1,
											position: 'static',
										}}
										leave={{
											transform: 'translate3d(0,-20px,0)',
											opacity: 0,
											position: 'static',
											display: 'none', // we want them to hide instantly
										}}
										config={config.stiff}
										native
										trail={50}
									>
										{form => props => (
											<Card
												key={form.id}
												title={form.name}
												subtitle={getCategoryName(
													form.category,
													cats
												)}
												style={props}
												actions={
													<>
														<CardAction
															href={form.url}
															onClick={e => {
																e.preventDefault();
																this.openModal(
																	form.id
																);
															}}
														>
															Preview
														</CardAction>
														<CardAction
															// eslint-disable-next-line camelcase
															href={`${download_url}?action=efl-download-form&form_id=${
																form.id
															}`}
														>
															Download
														</CardAction>
													</>
												}
											/>
										)}
									</Transition>
								</CardWrap>
							</SidebarLayout>
							<Modal
								open={modalOpen}
								closeAction={this.closeModal}
								id={
									currentModalForm
										? currentModalForm.id
										: null
								}
								url={
									currentModalForm
										? currentModalForm.url
										: null
								}
								title={
									currentModalForm
										? currentModalForm.name
										: null
								}
								nextAction={
									nextModalFormId
										? () => this.openModal(nextModalFormId)
										: null
								}
								prevAction={
									prevModalFormId
										? () => this.openModal(prevModalFormId)
										: null
								}
								download={
									currentModalForm
										? // eslint-disable-next-line camelcase
										  `${download_url}?action=efl-download-form&form_id=${
												currentModalForm.id
										  }`
										: null
								}
							/>
						</>
					)}
				</NeedCollapse>
			</AppTheme>
		);
	}
}

export default hot(Forms);
