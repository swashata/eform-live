import * as React from 'react';
import styled from 'styled-components';
import { Transition, config, animated, Trail } from 'react-spring';
import { disableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import classNames from 'classnames';
import Swipeable from 'react-swipeable';

import IFrame from '../IFrame';
import HoverSpin from '../HoverSpin';
import TimesIcon from '../icons/times.svg';
import ExpandIcon from '../icons/expand.svg';
import CollapseIcon from '../icons/collapse.svg';

const ModalOuter = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	height: 100vh;
	width: 100vw;
	background: rgba(0, 0, 0, 0.7);
	z-index: 99999;
	-webkit-overflow-scrolling: touch;
	overflow: auto;
`;

const ModalWrap = animated(
	styled.div`
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 99999;
		margin: auto;
		background: ${props => props.theme.background};
		box-shadow: 0 6px 14px rgba(0, 0, 0, 0.22);
		overflow: hidden;
		display: flex;
		flex-flow: column nowrap;
		transition: width 300ms ease-out, top 300ms ease-out,
			bottom 300ms ease-out;
		height: auto;
		width: 100vw;
		will-change: top, bottom, width, tranform;
		@media screen and (min-width: ${props =>
				props.theme.breakpointDesktop}) {
			top: 40px;
			bottom: 40px;
			width: ${props => props.maxWidth || '500px'};
			border-radius: 8px;
		}
		@media screen and (min-height: 1024px) and (min-width: ${props =>
				props.theme.breakpointDesktop}) {
			top: 100px;
			bottom: 100px;
		}
		&.efl-ra-form-modal__wrap--isfullscreen {
			border-radius: 0;
			top: 0;
			bottom: 0;
			width: 100vw;
		}
	`
);

const ModalHeader = styled.header`
	background: ${props => props.theme.primary};
	color: ${props => props.theme.primaryText};
	padding: 0 20px;
	height: 65px;
	display: flex;
	align-items: center;
	flex: 0 0 auto;

	.efl-ra-form-modal__title {
		width: calc(100% - 72px);
		flex: 0 0 auto;
	}
	.efl-ra-form-modal__size {
		width: 36px;
		margin-left: auto;
		display: none;
		@media screen and (min-width: ${props =>
				props.theme.breakpointDesktop}) {
			display: block;
		}
	}
	.efl-ra-form-modal__close {
		width: 36px;
		margin-left: auto;
		@media screen and (min-width: ${props =>
				props.theme.breakpointDesktop}) {
			margin-left: 0;
		}
	}
`;
const ModalTitle = styled.h2`
	font-size: 26px;
	font-weight: bold;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
`;

const ModalHeaderBtn = styled.button`
	height: 28px;
	width: 28px;
	border-radius: 100%;
	background-color: ${props => props.theme.primaryDark};
	color: ${props => props.theme.primaryText};
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 26px;
	margin: 0;
	padding: 0;
	border: 0 none;
	cursor: pointer;
	transition: background-color 200ms ease-out;

	&:hover {
		background-color: ${props => props.theme.primaryLight};
	}

	&:active,
	&:focus {
		box-shadow: 0 0 0 1px ${props => props.theme.primaryFocus};
		outline: none;
	}
`;

const ModalHeaderAnimatedBtn = animated(ModalHeaderBtn);

const ModalContent = styled.section`
	flex: 1 0 auto;
	position: relative;

	.efl-ra-form-modal__content {
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
		display: block;
	}
`;

const ModalFooter = styled.footer`
	background-color: ${props => props.theme.whiteShadeOne};
	height: 48px;
	display: flex;
	margin-top: auto;
	flex: 0 0 auto;
	align-items: center;
	padding: 0 18px;

	.efl-ra-form-modal__prev,
	.efl-ra-form-modal__next {
		width: 60px;
	}
	.efl-ra-form-modal__prev {
		margin-left: auto;
	}
	.efl-ra-form-modal__next {
		margin-left: 6px;
	}
`;

const ModalFooterBtn = styled.button`
	height: 32px;
	padding: 0 12px;
	min-width: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 14px;
	border: 0 none;
	background-color: transparent;
	text-transform: uppercase;
	color: ${props => props.theme.textLight};
	cursor: pointer;
	transition: color 200ms ease-out, background-color 200ms ease-out;
	will-change: color, background-color;
	border-radius: 4px;
	&:hover {
		background-color: ${props => props.theme.whiteShadeFour};
		color: ${props => props.theme.text};
	}
	&:active,
	&:focus {
		box-shadow: 0 0 0 1px ${props => props.theme.whiteShadeFive};
		outline: none;
	}
	&:disabled {
		cursor: not-allowed;
		color: ${props => props.theme.whiteShadeFour};
		background-color: transparent;
	}
`;

export default class Modal extends React.Component {
	static getDerivedStateFromProps(props, state) {
		// If we are closing the modal, then just reset the goindPrev
		// because next time, we start on default
		if (!props.open) {
			return {
				goingPrev: false,
			};
		}
		return null;
	}

	constructor(...args) {
		super(...args);
		this.modalRef = React.createRef();
		this.state = {
			isFullScreen: false,
			goingPrev: false,
		};
	}

	componentDidUpdate() {
		const { open } = this.props;
		if (open) {
			document.addEventListener('keydown', this.handleOuterKey);
		} else {
			document.removeEventListener('keydown', this.handleOuterKey);
		}
	}

	toggleFullScreen = () =>
		this.setState(({ isFullScreen }) => ({ isFullScreen: !isFullScreen }));

	getTrailComponents = items => {
		const { open } = this.props;
		const { goingPrev } = this.state;
		const disappearStyle = goingPrev
			? 'translateX(-40px)'
			: 'translateX(40px)';
		return (
			<Trail
				items={items}
				keys={i => i.key}
				native
				from={{
					transform: disappearStyle,
					opacity: 0,
				}}
				to={{
					transform: open ? 'translateX(0px)' : disappearStyle,
					opacity: open ? 1 : 0,
				}}
				config={config.wobbly}
			>
				{item => style => (
					<animated.div style={style} className={item.className}>
						{item.component}
					</animated.div>
				)}
			</Trail>
		);
	};

	handleOuterClick = e => {
		// If it is coming from the main modal
		// then don't do anything
		const { target } = e;
		const { closeAction } = this.props;
		if (
			target.classList &&
			target.classList.contains('efl-ra-form-modal__wrap')
		) {
			return;
		}
		if (target.closest && target.closest('.efl-ra-form-modal__wrap')) {
			return;
		}
		closeAction();
	};

	handleOuterKey = e => {
		const { closeAction } = this.props;
		switch (e.keyCode) {
			// Close on esc
			case 27:
				closeAction();
				break;

			// prevAction on left arrow
			case 37:
				this.goPrev();
				break;

			// nextAction on right arrow
			case 39:
				this.goNext();
				break;
			default:
			// do nothing
		}
	};

	goPrev = () => {
		const { prevAction } = this.props;
		if (prevAction) {
			this.setState({ goingPrev: true }, prevAction);
		}
	};

	goNext = () => {
		const { nextAction } = this.props;
		if (nextAction) {
			this.setState({ goingPrev: false }, nextAction);
		}
	};

	render() {
		const {
			open,
			id,
			url,
			title,
			download,
			nextAction,
			prevAction,
			closeAction,
			maxWidth = '600px',
		} = this.props;

		const { isFullScreen } = this.state;

		const headerTrailItems = [
			{
				component: <ModalTitle>{title}</ModalTitle>,
				key: `${id}-title`,
				className: 'efl-ra-form-modal__title',
			},
			{
				component: (
					<HoverSpin rotate={360}>
						{props => (
							<ModalHeaderAnimatedBtn
								{...props}
								onClick={this.toggleFullScreen}
								type="button"
							>
								{isFullScreen ? (
									<CollapseIcon />
								) : (
									<ExpandIcon />
								)}
							</ModalHeaderAnimatedBtn>
						)}
					</HoverSpin>
				),
				key: `${id}-sizebtn`,
				className: 'efl-ra-form-modal__size',
			},
			{
				component: (
					<HoverSpin>
						{props => (
							<ModalHeaderAnimatedBtn
								{...props}
								onClick={closeAction}
								type="button"
							>
								<TimesIcon />
							</ModalHeaderAnimatedBtn>
						)}
					</HoverSpin>
				),
				key: `${id}-closebtn`,
				className: 'efl-ra-form-modal__close',
			},
		];

		const contentTrailItems = [
			{
				component: <IFrame title={id} url={url} />,
				key: `${id}-content`,
				className: 'efl-ra-form-modal__content',
			},
		];

		const footerTrailItems = [
			{
				component: (
					<ModalFooterBtn as="a" href={download}>
						Download
					</ModalFooterBtn>
				),
				key: `${id}-download`,
				className: 'efl-ra-form-modal__download',
			},
			{
				component: (
					<ModalFooterBtn
						disabled={!prevAction}
						onClick={this.goPrev}
						type="button"
					>
						Prev
					</ModalFooterBtn>
				),
				key: `${id}-prev`,
				className: 'efl-ra-form-modal__prev',
			},
			{
				component: (
					<ModalFooterBtn
						disabled={!nextAction}
						onClick={this.goNext}
						type="button"
					>
						Next
					</ModalFooterBtn>
				),
				key: `${id}-next`,
				className: 'efl-ra-form-modal__next',
			},
		];

		return (
			<Transition
				native
				items={open}
				from={{
					opacity: 1,
					transform:
						'translateY(-20px) perspective( 600px ) rotateX( 10deg )',
				}}
				enter={{
					opacity: 1,
					transform:
						'translateY(0) perspective( 600px ) rotateX( 0deg )',
				}}
				leave={{
					opacity: 0,
				}}
				onStart={() => {
					const { open: _open } = this.props;
					if (_open) {
						disableBodyScroll(this.modalRef.current);
					} else {
						clearAllBodyScrollLocks();
					}
				}}
				config={open ? config.gentle : { tension: 700, friction: 50 }}
			>
				{show =>
					show &&
					(props => (
						<Swipeable
							onSwipedLeft={this.goNext}
							onSwipedRight={this.goPrev}
						>
							<ModalOuter
								ref={this.modalRef}
								className="efl-ra-form-modal"
								onClick={this.handleOuterClick}
							>
								<ModalWrap
									style={props}
									maxWidth={maxWidth}
									className={classNames(
										'efl-ra-form-modal__wrap',
										{
											'efl-ra-form-modal__wrap--isfullscreen': isFullScreen,
										}
									)}
								>
									<ModalHeader className="efl-ra-form-modal__header">
										{this.getTrailComponents(
											headerTrailItems
										)}
									</ModalHeader>
									<ModalContent>
										{this.getTrailComponents(
											contentTrailItems
										)}
									</ModalContent>
									<ModalFooter>
										{this.getTrailComponents(
											footerTrailItems
										)}
									</ModalFooter>
								</ModalWrap>
							</ModalOuter>
						</Swipeable>
					))
				}
			</Transition>
		);
	}
}
