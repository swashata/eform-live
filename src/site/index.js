// Styles and scripts for archive page
// Import styles
import './style.scss';

// Import commons
import '../components/nav';

// Import archive specifics
import '../components/page-components';
import '../components/bullhorn';
import '../components/breadcrumb';
import '../components/content-card';
import '../components/socialmenu';
import '../components/footer';
