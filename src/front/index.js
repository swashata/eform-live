// Import styles
import './style.scss';

// Import commons
import '../components/nav';
import '../gutenberg/feature/front';

// Import front-page specifics
import '../components/hero';
import '../components/brands';
import '../components/steps';
import '../components/testimonial';
import '../components/livevideo';
import '../components/cards';
import '../components/socialmenu';
import '../components/footer';
