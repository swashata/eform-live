import { registerBlockType } from '@wordpress/blocks';
import { Feature, FeatureBlockName } from './feature';

registerBlockType(FeatureBlockName, Feature);
