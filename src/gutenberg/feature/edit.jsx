import * as React from 'react';
import classNames from 'classnames';
import { Button, Toolbar, ButtonGroup } from '@wordpress/components';
import { RichText, MediaUpload, InspectorControls } from '@wordpress/editor';
// import { hot } from 'react-hot-loader';
import { __ } from '@wordpress/i18n';

import SubContent from './components/SubContent';
import Cta from './components/Cta';

import placeholderImg from './placeholder.png';

const Edit = props => {
	const { attributes, setAttributes, className, isSelected } = props;
	const {
		heading,
		content,
		imageUrl,
		imageId,
		imageHeight,
		imageWidth,
		blockSize,
	} = attributes;

	const sizes = {
		xs: __('XS'),
		s: __('S'),
		m: __('M'),
		l: __('L'),
	};

	return (
		<React.Fragment>
			<InspectorControls>
				<p className="description">
					{__('Customize the look and feel of this block.')}
				</p>
				<p>{__('Heading Size')}</p>
				<ButtonGroup>
					{Object.keys(sizes).map(size => (
						<Button
							key={size}
							isToggled
							isPrimary={size === blockSize}
							isSmall
							onClick={() => setAttributes({ blockSize: size })}
						>
							{sizes[size]}
						</Button>
					))}
				</ButtonGroup>
			</InspectorControls>
			<div
				className={classNames(
					className,
					'efl-feature',
					'efl-feature--edit-widget',
					{
						'efl-feature--isSelected': isSelected,
					}
				)}
			>
				<RichText
					tagName="h2"
					className={classNames(
						'efl-feature__title',
						`efl-feature__title--${blockSize}`
					)}
					onChange={val => setAttributes({ heading: val })}
					value={heading}
					placeholder={__('Block title')}
				/>
				<RichText
					tagName="div"
					className="efl-feature__content"
					onChange={val => setAttributes({ content: val })}
					value={content}
					placeholder={__('Enter block description here')}
				/>
				<MediaUpload
					type="image"
					value={imageId}
					allowedTypes={['image']}
					onSelect={media => {
						setAttributes({
							imageId: media.id,
							imageUrl: media.url,
							imageHeight: media.height,
							imageWidth: media.width,
						});
					}}
					render={({ open }) => (
						<div className="efl-feature__image">
							<img
								src={imageUrl || placeholderImg}
								alt={heading}
								height={imageHeight}
								width={imageWidth}
								className="efl-feature__img"
							/>
							<Button isLarge isPrimary onClick={open}>
								{imageUrl
									? __('Change Image')
									: __('Upload Image')}
							</Button>
						</div>
					)}
				/>
				<div className="efl-feature__subcontents">
					{['one', 'two', 'three'].map(sub => (
						<SubContent
							isWidget
							content={attributes[`subcontent_${sub}`]}
							key={sub}
							heading={attributes[`subtitle_${sub}`]}
							num={sub}
							setContent={val =>
								setAttributes({
									[`subcontent_${sub}`]: val,
								})
							}
							setHeading={val =>
								setAttributes({
									[`subtitle_${sub}`]: val,
								})
							}
						/>
					))}
				</div>
				<div className="efl-feature__ctas">
					{['one', 'two'].map(but => (
						<Cta
							isWidget
							key={but}
							num={but}
							isSelected={isSelected}
							label={attributes[`cta_${but}_text`]}
							url={attributes[`cta_${but}_link`]}
							setLabel={val =>
								setAttributes({
									[`cta_${but}_text`]: val,
								})
							}
							setUrl={val =>
								setAttributes({
									[`cta_${but}_link`]: val,
								})
							}
						/>
					))}
				</div>
			</div>
		</React.Fragment>
	);
};

export default Edit;
