import * as React from 'react';
import classNames from 'classnames';
import { RichText, URLInput } from '@wordpress/editor';
import { Dashicon } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

const Cta = ({
	isSelected,
	num,
	url,
	label,
	setUrl,
	setLabel,
	isWidget = false,
}) => {
	const className = classNames(
		'efl-feature__cta',
		`efl-feature__cta--${num}`,
		'efl-cta-btn',
		{
			'efl-cta-btn--solid': num === 'two',
		}
	);
	return isWidget ? (
		<div className="efl-feature__ctaeditor efl-cta-btn-group">
			<RichText
				value={label}
				onChange={setLabel}
				tagName="div"
				className={className}
				placeholder={__('Button label')}
			/>
			<form
				className="block-library-button__inline-link"
				onSubmit={event => event.preventDefault()}
			>
				<Dashicon icon="admin-links" />
				<URLInput value={url} onChange={setUrl} />
			</form>
		</div>
	) : (
		<RichText.Content
			tagName="a"
			href={url}
			className={className}
			value={label}
		/>
	);
};

export default Cta;
