import * as React from 'react';
import classNames from 'classnames';
import { RichText } from '@wordpress/editor';
import { __ } from '@wordpress/i18n';

const SubContent = ({
	num,
	heading,
	setHeading,
	content,
	setContent,
	isWidget = false,
}) => {
	const TextComponent = isWidget ? RichText : RichText.Content;
	const titleProps = {
		tagName: 'h3',
		className: 'efl-feature__sub-title',
		value: heading,
		placeholder: __('Subtitle'),
	};
	const paraProps = {
		tagName: 'p',
		className: 'efl-feature__sub-para',
		value: content,
		placeholder: __('enter description here'),
	};
	if (isWidget) {
		titleProps.onChange = setHeading;
		paraProps.onChange = setContent;
	}
	return (
		<div
			className={classNames(
				'efl-feature__sub',
				`efl-feature__sub--${num}`
			)}
		>
			<TextComponent {...titleProps} />
			<TextComponent {...paraProps} />
		</div>
	);
};

export default SubContent;
