import * as React from 'react';
import classNames from 'classnames';
import { RichText } from '@wordpress/editor';

import SubContent from './components/SubContent';
import Cta from './components/Cta';
import placeholderImg from './placeholder.png';

const Save = props => {
	const { attributes, className } = props;
	const {
		heading,
		content,
		imageUrl,
		imageHeight,
		imageWidth,
		blockSize,
	} = attributes;

	return (
		<div className={classNames('efl-feature', className)}>
			<RichText.Content
				tagName="h2"
				className={classNames(
					'efl-feature__title',
					`efl-feature__title--${blockSize}`
				)}
				value={heading}
			/>
			<RichText.Content
				tagName="div"
				className="efl-feature__content"
				value={content}
			/>
			<div className="efl-feature__image">
				<img
					src={imageUrl || placeholderImg}
					alt={heading}
					height={imageHeight || 530}
					width={imageWidth || 944}
					className="efl-feature__img"
				/>
			</div>
			<div className="efl-feature__subcontents">
				{['one', 'two', 'three'].map(sub => (
					<SubContent
						content={attributes[`subcontent_${sub}`]}
						key={sub}
						heading={attributes[`subtitle_${sub}`]}
						num={sub}
					/>
				))}
			</div>
			<div className="efl-feature__ctas">
				{['one', 'two'].map(but => (
					<Cta
						key={but}
						num={but}
						label={attributes[`cta_${but}_text`]}
						url={attributes[`cta_${but}_link`]}
					/>
				))}
			</div>
		</div>
	);
};

export default Save;
