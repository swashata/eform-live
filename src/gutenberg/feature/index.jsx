import * as React from 'react';

import Edit from './edit';
import Save from './save';

import './style.scss';
import './editor.scss';

export const Feature = {
	title: 'Feature Block',
	icon: 'align-center',
	category: 'layout',
	attributes: {
		// Things we can extract from our HTML
		heading: {
			type: 'string',
			source: 'html',
			selector: 'h2.efl-feature__title',
			default: 'Lorem ipsum dolor sit amet, consectetur adipiscing',
		},
		content: {
			type: 'string',
			source: 'html',
			selector: 'div.efl-feature__content',
			default:
				'Nulla facilisi. Praesent posuere malesuada ex, vel suscipit ligula scelerisque et. Nunc aliquam eleifend mauris, sed dignissim ipsum aliquam ac. Cras ornare metus ut semper ultrices. Mauris ut.',
		},
		imageUrl: {
			type: 'string',
			source: 'attribute',
			selector: 'div.efl-feature__image img',
			attribute: 'src',
		},
		imageHeight: {
			type: 'string',
			source: 'attribute',
			selector: 'div.efl-feature__image img',
			attribute: 'height',
		},
		imageWidth: {
			type: 'string',
			source: 'attribute',
			selector: 'div.efl-feature__image img',
			attribute: 'width',
		},
		subtitle_one: {
			type: 'string',
			source: 'text',
			selector: 'div.efl-feature__sub--one > h3',
			default: 'Pointer One',
		},
		subtitle_two: {
			type: 'string',
			source: 'text',
			selector: 'div.efl-feature__sub--two > h3',
			default: 'Pointer Two',
		},
		subtitle_three: {
			type: 'string',
			source: 'text',
			selector: 'div.efl-feature__sub--three > h3',
			default: 'Just Three',
		},
		subcontent_one: {
			type: 'string',
			source: 'html',
			selector: 'div.efl-feature__sub--one > p',
			default:
				'This is a sample text for testing purpose only. Do not think this as a real content of the it.',
		},
		subcontent_two: {
			type: 'string',
			source: 'html',
			selector: 'div.efl-feature__sub--two > p',
			default:
				'This is a sample text for testing purpose only. Do not think this as a real content of the it.',
		},
		subcontent_three: {
			type: 'string',
			source: 'html',
			selector: 'div.efl-feature__sub--three > p',
			default:
				'This is a sample text for testing purpose only. Do not think this as a real content of the it.',
		},
		cta_one_link: {
			type: 'string',
			source: 'attribute',
			selector: 'a.efl-feature__cta--one',
			attribute: 'href',
		},
		cta_one_text: {
			type: 'string',
			source: 'text',
			selector: 'a.efl-feature__cta--one',
			default: 'Examples',
		},
		cta_two_link: {
			type: 'string',
			source: 'attribute',
			selector: 'a.efl-feature__cta--two',
			attribute: 'href',
		},
		cta_two_text: {
			type: 'string',
			source: 'html',
			selector: 'a.efl-feature__cta--two',
			default: 'Buy',
		},

		// Things stored in config and attributes in
		// appearance of block
		imageId: {
			type: 'string',
		},
		blockSize: {
			type: 'string',
			default: 'l',
		},
	},
	edit(props) {
		return <Edit {...props} />;
	},
	save(props) {
		return <Save {...props} />;
	},
};

export const FeatureBlockName = 'eform-live-theme/feature';
