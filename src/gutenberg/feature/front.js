import VanillaTilt from 'vanilla-tilt';

document.addEventListener('DOMContentLoaded', () => {
	VanillaTilt.init(document.querySelectorAll('.efl-feature__image'), {
		scale: 1.01,
		glare: 1,
		max: 2,
		'max-glare': 0.4,
	});
});
