import VanillaTilt from 'vanilla-tilt';

import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
	VanillaTilt.init(document.querySelectorAll('.efl-fp-steps__tiltable'), {
		scale: 1.05,
		max: 12,
	});
});
