// We need a few libraries for animation
import video from './video';
import animateSVG from './svg';

// Import the style
import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
	video();
	animateSVG();
});
