import animejs from 'animejs';

export default function animateSVG() {
	const selectors = [
		'path',
		'rect',
		'polyline',
		'ellipse',
		'circle',
		'polygon',
	];
	const heroSvgs = document.querySelectorAll(
		'.efl-fp-hero__background-svg svg'
	);
	heroSvgs.forEach((svg, index) => {
		const dashElements = svg.querySelectorAll(selectors.join(','));
		const delay = index * 500;
		animejs({
			targets: dashElements,
			strokeDashoffset: [animejs.setDashoffset, 0],
			easing: 'easeInOutSine',
			duration: 1200,
			delay: (el, i) => delay + i * 150,
			direction: 'alternate',
			loop: true,
		});
	});
}
