import animejs from 'animejs';

const startVideo = () => {
	const videoScreen = document.querySelector('.efl-fp-hero__video-frame');
	videoScreen.innerHTML = `<div class='efl-fp-hero__embed-container'><iframe src="https://www.youtube.com/embed/${
		videoScreen.dataset.id
	}?rel=0&amp;autoplay=1" frameborder="0" allow="autoplay" allowfullscreen></iframe></div>`;
};

const stopVideo = () => {
	const videoScreen = document.querySelector('.efl-fp-hero__video-frame');
	videoScreen.innerHTML = '';
};

const video = () => {
	const heroFrame = document.querySelector('.efl-fp-hero');
	// Create the timeline
	const videoTimelineReveal = animejs
		.timeline({
			autoplay: false,
		})
		// Hide everything
		.add({
			targets: document.querySelector('.efl-fp-hero__video'),
			easing: 'easeInExpo', // [0.74, -1.17, 0.93, 0.89],
			duration: 200,
			scale: 0,
		})
		.add({
			targets: document.querySelector('.efl-fp-hero__ctas'),
			easing: 'easeInExpo',
			scale: 0,
			duration: 300,
			offset: '-=300',
		})
		.add({
			targets: document.querySelector('.efl-fp-hero__subtitle'),
			easing: 'easeInExpo',
			duration: 300,
			scale: 0,
			offset: '-=200',
		})
		.add({
			targets: document.querySelector('.efl-fp-hero__title'),
			easing: 'easeInExpo',
			duration: 200,
			scale: 0,
			offset: '-=200',
			complete: () => {
				heroFrame.classList.add('efl-fp-hero--video-active');
				startVideo();
			},
		});

	// Show the revealing animation on click
	document
		.querySelector('.efl-fp-hero__video-anchor')
		.addEventListener('click', e => {
			e.preventDefault();
			videoTimelineReveal.restart();
		});
	document
		.querySelector('.efl-fp-hero__video-close')
		.addEventListener('click', e => {
			e.preventDefault();
			stopVideo();
			heroFrame.classList.remove('efl-fp-hero--video-active');
			videoTimelineReveal.play();
			videoTimelineReveal.reverse();
		});
};

export default video;
