import VanillaTilt from 'vanilla-tilt';

import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
	document
		.querySelector('.efl-fp-cards__more')
		.addEventListener('click', e => {
			e.preventDefault();
			document
				.querySelector('.efl-fp-cards')
				.classList.add('efl-fp-cards--is-shown');
		});
	VanillaTilt.init(document.querySelectorAll('.efl-fp-cards__item'), {
		scale: 1,
		max: 30,
		glare: true,
		'max-glare': 0.4,
	});
});
