import Siema from 'siema';
import { requestInterval, cancelInterval } from '../../utils/interval';

import './index.scss';

document.addEventListener(
	'DOMContentLoaded',
	() =>
		setTimeout(() => {
			const pagerDiv = document.querySelector(
				'.efl-fp-testimonial__pager'
			);
			const allItems = document.querySelectorAll(
				'.efl-fp-testimonial__item'
			).length;
			const siemaContainer = document.querySelector(
				'.efl-fp-testimonial__siema'
			);

			// Init
			const testimonial = new Siema({
				selector: siemaContainer,
				duration: 500,
				perPage: 1,
				startIndex: 0,
				loop: true,
				onChange() {
					const current = this.currentSlide;
					pagerDiv
						.querySelectorAll('.efl-fp-testimonial__page')
						.forEach((page, index) => {
							if (index === current) {
								page.classList.add(
									'efl-fp-testimonial__page--active'
								);
							} else {
								page.classList.remove(
									'efl-fp-testimonial__page--active'
								);
							}
						});
				},
				onInit() {
					siemaContainer.classList.add(
						'efl-fp-testimonial__siema--activated'
					);
				},
			});

			// Autoplay pause on events
			let loopHandle = requestInterval(() => {
				testimonial.next();
			}, 2000);
			let isRunning = true;

			const stopLoop = () => {
				if (isRunning) {
					cancelInterval(loopHandle);
					isRunning = false;
				}
			};

			const startLoop = () => {
				if (!isRunning) {
					loopHandle = requestInterval(() => {
						testimonial.next();
					}, 2000);
					isRunning = true;
				}
			};

			['mousedown', 'touchstart', 'touchmove'].forEach(event => {
				siemaContainer.addEventListener(event, stopLoop);
			});

			siemaContainer.addEventListener('dblclick', startLoop);

			// Nav
			const nextButton = document.querySelector(
				'.efl-fp-testimonial__nav-right'
			);
			const prevButton = document.querySelector(
				'.efl-fp-testimonial__nav-left'
			);

			nextButton.addEventListener('click', e => {
				e.preventDefault();
				stopLoop();
				testimonial.next();
			});
			prevButton.addEventListener('click', e => {
				e.preventDefault();
				stopLoop();
				testimonial.prev();
			});

			// Pager
			for (let i = 0; i < allItems; i++) {
				const pager = document.createElement('a');
				pager.classList.add('efl-fp-testimonial__page');
				if (i === 0) {
					pager.classList.add('efl-fp-testimonial__page--active');
				}
				pager.setAttribute('href', '#');
				pager.dataset.index = i;
				pagerDiv.appendChild(pager);
			}
			pagerDiv.addEventListener('click', e => {
				e.preventDefault();
				stopLoop();
				if (e.target) {
					const { index } = e.target.dataset;
					testimonial.goTo(index);
				}
			});
		}),
	// A little hack so that it plays nice with our css-loader
	// since SIEMA is applied before css-loader is able to
	// put all the CSS in the page.
	process.env.NODE_ENV === 'development' ? 2000 : 10
);
