import scrollMonitor from 'scrollmonitor';

import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
	const videoEmbed = document.querySelector('.efl-fp-video__embed');
	const videoTrigger = document.querySelector('.efl-fp-video__trigger');
	let hasVideoPlayed = false;
	const playVideo = () => {
		videoEmbed.play().then(() => {
			videoTrigger.style.display = 'none';
			hasVideoPlayed = true;
		});
	};
	videoTrigger.addEventListener('click', e => {
		e.preventDefault();
		playVideo();
	});
	const resetVideo = () => {
		if (!hasVideoPlayed) {
			return;
		}
		videoEmbed.pause();
		videoEmbed.currentTime = 0;
		videoEmbed.play();
		// That's how we reset
	};

	const videoWatcher = scrollMonitor.create(videoEmbed, 100);
	videoWatcher.one('enterViewport', playVideo);
	videoWatcher.on('enterViewport', resetVideo);
});
