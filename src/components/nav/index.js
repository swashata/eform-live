// Import nav styling
import './index.scss';

// Initiate the nav
document.addEventListener('DOMContentLoaded', () => {
	const navBurger = document.querySelector('.efl-site-header__hamburger');
	const body = document.querySelector('body');
	const navOverflow = document.querySelector('.efl-site-header__navoverflow');
	const showNav = () => {
		body.classList.add('efl-nav-active');
		navBurger.classList.add('is-active');
	};
	const hideNav = () => {
		navBurger.classList.remove('is-active');
		body.classList.remove('efl-nav-active');
	};
	if (navBurger) {
		navBurger.addEventListener('click', e => {
			e.preventDefault();
			if (navBurger.classList.contains('is-active')) {
				// Collapse
				hideNav();
			} else {
				// Show
				showNav();
			}
		});
	}
	if (navOverflow) {
		navOverflow.addEventListener('click', () => {
			if (navBurger) {
				hideNav();
			}
		});
	}
});
