const pkg = require('./package.json');

// Define WordPress dependencies
const externals = {
	jquery: 'jQuery',
	react: 'React',
	'react-dom': 'ReactDOM',
};
const wpDependencies = ['components', 'element', 'blocks', 'i18n', 'editor'];
// Setup externals for all WordPress dependencies
wpDependencies.forEach(wpDependency => {
	externals[`@wordpress/${wpDependency}`] = ['wp', wpDependency];
});

module.exports = {
	// Project Identity
	appName: 'eformLive', // Unique name of your project
	type: 'theme', // Plugin or theme
	slug: 'eform-live', // Plugin or Theme slug, basically the directory name under `wp-content/<themes|plugins>`
	// Used to generate banners on top of compiled stuff
	bannerConfig: {
		name: 'eformLive',
		author: 'Swashata Ghosh',
		license: 'GPL-3.0',
		link: 'GPL-3.0',
		version: pkg.version,
		copyrightText:
			'This software is released under the GPL-3.0 License\nhttps://opensource.org/licenses/GPL-3.0',
		credit: true,
	},
	// Files we need to compile, and where to put
	files: [
		// Website front-page, super optimized assets
		{
			name: 'front',
			entry: {
				main: ['./src/front/index.js'],
			},
			// Extra webpack config to be passed directly
			webpackConfig: undefined,
		},
		// For the rest of the site
		{
			name: 'site',
			entry: {
				main: ['./src/site/index.js'],
			},
			// Extra webpack config to be passed directly
			webpackConfig: undefined,
		},
		// Gutenberg blocks
		{
			name: 'gutenberg',
			entry: {
				main: ['./src/gutenberg/main.js'],
			},
			// Extra webpack config to be passed directly
			webpackConfig: undefined,
		},
		// Shortcodes
		{
			name: 'react-apps',
			entry: {
				forms: ['./src/react-apps/forms.jsx'],
				themes: ['./src/react-apps/themes.jsx'],
			},
			webpackConfig: (config, merge) => {
				// Override the externals, because we will need
				// react as-is
				const newConfig = { ...config, externals: {} };
				// Use svgr for SVG in react
				newConfig.module.rules = [
					...newConfig.module.rules,
					// Use @svgr/webpack for SVGs from jsx or tsx files
					{
						test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
						issuer: {
							test: /\.(j|t)sx?$/,
						},
						use: [
							{
								loader: '@svgr/webpack',
								options: {
									icon: true,
								},
							},
						],
					},
					// Use file-loader for everything else
					{
						test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
						issuer: {
							not: [/\.(j|t)sx?$/],
						},
						use: ['file-loader'],
					},
				];
				return newConfig;
			},
		},
	],
	// Output path relative to the context directory
	// We need relative path here, else, we can not map to publicPath
	outputPath: 'dist',
	// Project specific config
	// Needs react(jsx)?
	hasReact: true,
	// Needs sass?
	hasSass: true,
	// Needs flowtype?
	hasFlow: false,
	// Externals
	// <https://webpack.js.org/configuration/externals/>
	externals,
	// Webpack Aliases
	// <https://webpack.js.org/configuration/resolve/#resolve-alias>
	alias: undefined,
	// Show overlay on development
	errorOverlay: true,
	// Auto optimization by webpack
	// Split all common chunks with default config
	// <https://webpack.js.org/plugins/split-chunks-plugin/#optimization-splitchunks>
	// Won't hurt because we use PHP to automate loading
	optimizeSplitChunks: true,
	// Usually PHP and other files to watch and reload when changed
	watch: [
		'./inc/**/*.php',
		'./*.php',
		'./page-templates/**',
		'./template-parts/**',
	],
	// Hook into babeloverride so that we can add react-hot-loader plugin
	// and styled components
	jsBabelOverride: defaults => ({
		...defaults,
		plugins: ['babel-plugin-styled-components', 'react-hot-loader/babel'],
	}),
	// Files that you want to copy to your ultimate theme/plugin package
	// Supports glob matching from minimatch
	// @link <https://github.com/isaacs/minimatch#usage>
	packageFiles: [
		'dist/**',
		'fonts/**',
		'images/**',
		'inc/**',
		'languages/**',
		'page-templates/**',
		'template-parts/**',
		'vendor/**',
		'*.php',
		'*.md',
		'readme.txt',
		'layouts/**',
		'LICENSE',
		'*.css',
		'screenshot.png',
	],
	// Path to package directory, relative to the root
	packageDirPath: 'package',
};
