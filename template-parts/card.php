<?php
/**
 * Template part for displaying archive cards.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 */

?>

<article title="<?php echo esc_attr( get_the_title() ); ?>" id="post-<?php the_ID(); ?>" <?php post_class( 'efl-content-card' ); ?>>
	<?php
	if ( has_post_thumbnail() ) {
		$image_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );
		echo '<div class="efl-content-card__bgimage"'
			. ' style="background-image: url(&quot;' . esc_attr( $image_url ) . '&quot;)"'
			. '></div>';
	}
	?>
	<div class="efl-content-card__gdoverlay"></div>
	<a class="efl-content-card__link" href="<?php the_permalink(); ?>">
		<header class="entry-header efl-content-card__header">
			<?php the_title( '<h2 class="entry-title efl-content-card__title">', '</h2>' ); ?>
		</header><!-- .entry-header -->

		<footer class="entry-footer efl-content-card__footer">
			<p class="efl-content-card__readmore">
				<?php _e( 'Read More &rsaquo;', 'eform-live' ); ?>
			</p>
			<div class="efl-content-card__date">
				<?php echo eform_live_get_entry_date(); ?>
			</div>
		</footer>
	</a>
</article><!-- #post-<?php the_ID(); ?> -->
