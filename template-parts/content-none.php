<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 */

?>

<article id="post-error-404" <?php post_class( 'entry' ); ?>>
	<div class="efl-page-entry efl-content efl-content--nomw">
		<div class="entry-content content">
			<p class="aligncenter" style="text-align: center"><?php _e( 'Now that\'s an error we would wish to avoid!', 'eform-live' ); ?></p>
			<div class="readmore efl-cta-btn-group efl-cta-btn-group--align-center">
				<a class="efl-cta-btn efl-cta-btn--small efl-cta-btn--solid efl-cta-btn--fluid" href="<?php echo home_url(); ?>"><?php _e( 'Go Home', 'eform-live' ); ?></a>
			</div>
		</div>
	</div><!-- .entry-content -->
	<?php eform_live_content_nav( 'nav-below' ); ?>
</article><!-- #post-## -->

