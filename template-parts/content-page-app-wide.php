<?php
/**
 * Template part for displaying wide app pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry efl-page-app efl-page-app--wide' ); ?>>
	<?php
	eform_live_page_heading(
		get_the_title(),
		false,
		false
	);
	?>
	<div class="efl-page-entry efl-content efl-content--nomw">
		<div class="app-content">
		<?php
		the_content(
			sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'eform-live' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			)
		);
		?>
		</div>
	</div><!-- .content -->
	<?php eform_live_content_nav( 'nav-below' ); ?>
</article><!-- #post-## -->
