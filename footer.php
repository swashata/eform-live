<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EFormLiveTheme
 */

?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="footer-nav" class="efl-section">
			<div class="efl-container">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer',
						'menu_id' => 'footer-nav-menu',
						'depth' => 1,
					)
				);
				?>
			</div>
		</div>
		<div id="footer-sections" class="efl-section">
			<div class="efl-container">
				<div class="efl-footer-sections">
					<aside class="efl-footer-sections__upper" role="complementary">
						<?php eform_live_social_menu(); ?>
					</aside>

					<aside class="efl-footer-sections__lower" role="complementary">
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'bottom',
									'menu_id' => 'footer-bottom-menu',
									'depth' => 1,
									'container' => false,
								)
							);
							?>
							<div class="site-info">
								<?php /* translators: %1$s and %2$s are replaced by current YEAR and blog name */ ?>
								<?php printf( __( '&copy; %1$s %2$s &dash; All Rights Reserved', 'eform-live' ), current_time( 'Y' ), '<a href="' . site_url( '/' ) . '">' . get_bloginfo( 'name' ) . '</a>' ); ?>
							</div><!-- .site-info -->
					</aside>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
