<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 * @subpackage Page Templates
 */

get_header();

/**
 * Print the front-page sections
 *
 * This holds true if user has selected to show a static front-page
 *
 * It is printed above the page content
 */
?>
<!-- Hero -->
<?php eform_live_frontpage_hero(); ?>
<!-- /Hero -->

<!-- Brands -->
<?php eform_live_frontpage_brands(); ?>
<!-- /Brands -->

<!-- Steps -->
<?php eform_live_frontpage_steps(); ?>
<!-- /Steps -->

<!-- Testimonials -->
<?php eform_live_frontpage_testimonials(); ?>
<!-- /Testimonials -->

<!-- Live Video -->
<?php eform_live_frontpage_livevideo(); ?>
<!-- /Live Video -->
<!-- Main features section, this comes from the page it-self -->
<section class="fp-section" id="site-sections">
	<main id="main" class="site-main efl-container" role="main">
	<?php
		the_post();
		the_content();
	?>
	</main><!-- #main -->
</section><!-- #site-sections -->
<!-- /Main Features -->

<!-- Cards -->
<?php eform_live_frontpage_cards(); ?>
<!-- /Cards -->
<?php
get_footer();
