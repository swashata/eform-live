const path = require('path');
module.exports = {
	extends: "@wpquark",
	settings: {
		"import/resolver": {
			webpack: {
				config: {
					resolve: {
						extensions: ['.js', '.jsx'],
						alias: {
							Styles: path.join(__dirname, 'scss/'),
						},
					}
				}
			}
		}
	},
	rules: {
		"react/prop-types": "off",
		"import/prefer-default-export": "off"
	}
};
