<?php
/**
 * Holds the class for live customizer and kirki integration.
 *
 * @package EFormLiveTheme
 */

/**
 * Class for customizer add-ons for easy theme management.
 *
 * Internally it uses kirki.
 */
class EForm_Live_Customizer {

	/**
	 * Kirki Theme Config Id.
	 *
	 * @var string
	 */
	private $config_id = 'eform_live_theme_op';

	/**
	 * Cache for all registered panels
	 *
	 * @var array
	 */
	private $panels = [];

	/**
	 * Cache for all registered sections
	 *
	 * @var array
	 */
	private $sections = [];

	/**
	 * Register Group ID in the internal system. It does not really registers
	 * anything with Kirki
	 *
	 * @param string $group The group under which to send this key. [panel, section].
	 * @param string $key Internal Group Key.
	 * @return void
	 */
	protected function register_group_id( $group, $key ) {
		$this->{$group}[ $key ] = 'eform_live_customizer_' . esc_attr( $group ) . '_' . esc_attr( $key );
	}

	/**
	 * Get the registered Id from a group.
	 *
	 * @param string $group The group, [panel, section].
	 * @param string $key The Group Key.
	 * @return string The registered Id.
	 */
	protected function get_group_id( $group, $key ) {
		return $this->{$group}[ $key ];
	}

	/**
	 * Register Panel ID in the internal system. It does not really registers
	 * a panel with Kirki
	 *
	 * @param string $key Internal Panel Key.
	 * @return string Panel Id.
	 */
	protected function register_panel_id( $key ) {
		$this->register_group_id( 'panels', $key );
		return $this->get_panel_id( $key );
	}

	/**
	 * Get Panel Id based on internal panel key.
	 *
	 * @param string $key Internal Panel Key.
	 * @return string Panel Id.
	 */
	protected function get_panel_id( $key ) {
		return $this->get_group_id( 'panels', $key );
	}

	/**
	 * Register Section ID in the internal system. It does not really registers
	 * a section with Kirki
	 *
	 * @param string $key Internal Section Key.
	 * @return void
	 */
	protected function register_section_id( $key ) {
		$this->register_group_id( 'sections', $key );
	}

	/**
	 * Get Section Id based on internal section key.
	 *
	 * @param string $key Internal Section Key.
	 * @return string Section Id.
	 */
	protected function get_section_id( $key ) {
		return $this->get_group_id( 'sections', $key );
	}

	/**
	 * Get the configuration Id.
	 *
	 * @return string The Kirki Config Id.
	 */
	protected function cid() {
		return $this->config_id;
	}

	/**
	 * Register a section to a panel and get the section id.
	 *
	 * @param string $panel_id Id of the panel.
	 * @param string $section_slug Slug of the section to be created.
	 * @param string $section_label Label of the section.
	 *
	 * @return string Section Id of the newly created section.
	 */
	protected function register_section_to_panel( $panel_id, $section_slug, $section_label ) {
		// Add a section
		$this->register_section_id( $section_slug );
		$section_id = $this->get_section_id( $section_slug );
		EForm_Live_Kirki::add_section(
			$section_id, [
				'title' => $section_label,
				'panel' => $panel_id,
			]
		);
		return $section_id;
	}

	/**
	 * Takes any field configuration array and pushes the default
	 * value from registry.
	 *
	 * @param array $config Field configuration array.
	 * @return array Configuration with replaced/added default from registry.
	 */
	protected function with_def( $config ) {
		if ( isset( $config['settings'] ) ) {
			$config['default'] = eform_live_get_theme_mod_default( $config['settings'] );
		}

		return $config;
	}

	/**
	 * Add field to kirki with this theme's CID and with default
	 * configuration value.
	 *
	 * @param array $config Field configuration array.
	 * @return void.
	 */
	protected function add_field( $config ) {
		EForm_Live_Kirki::add_field(
			$this->cid(),
			$this->with_def( $config )
		);
	}

	/**
	 * Create an instance.
	 *
	 * Hook into Kirki APIs.
	 */
	public function __construct() {
		// Call the stuff
		$this->register_theme_config();
		$this->register_all_configs();
	}

	/**
	 * Register EForm Theme options with Kirki.
	 *
	 * @return void
	 */
	public function register_theme_config() {
		EForm_Live_Kirki::add_config(
			$this->cid(), [
				'capability' => 'edit_theme_options',
				'option_type' => 'theme_mod',
			]
		);
	}

	/**
	 * Register all panels, sections and controls for customizer extensions.
	 *
	 * @return void
	 */
	public function register_all_configs() {
		// Frontpage
		$this->register_frontpage_configs();
		// Menu
		$this->register_menu_configs();
		// Pages
		$this->register_page_configs();
	}

	/**
	 * Register modifications for the top navigation.
	 * We intend to place a CTA there.
	 *
	 * @return void
	 */
	public function register_menu_configs() {
		// Panel is from the core
		$panel_id = 'nav_menus';
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'efl_nav',
			__( 'Theme Menu Mods', 'eform-live' )
		);
		$this->add_field( [
			'section' => $section_id,
			'label' => __( 'CTA Label', 'eform-live' ),
			'settings' => 'nav_cta_label',
			'type' => 'text',
		] );
		$this->add_field( [
			'section' => $section_id,
			'label' => __( 'CTA Link', 'eform-live' ),
			'settings' => 'nav_cta_url',
			'type' => 'link',
		] );
		$socials = eform_live_get_available_socials();

		foreach ( $socials as $key => $label ) {
			$this->add_field( [
				'section' => $section_id,
				'label' => $label,
				'settings' => "social_{$key}",
				'type' => 'link',
			] );
		}
	}

	/**
	 * Register Single page related settings, panels and controls.
	 *
	 * @return void.
	 */
	public function register_page_configs() {
		// Add the panel
		$panel_id = $this->register_panel_id( 'sp' );
		EForm_Live_Kirki::add_panel(
			$panel_id, [
				'priority' => 10,
				'title' => __( 'Page Configuration', 'eform-live' ),
				'description' => __( 'Configure Different Sections of Page excluding front-page.', 'eform-live' ),
				'active_callback' => 'eform_live_is_not_frontpage',
			]
		);

		// Add the only section we need right now
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'efl_attr',
			__( 'Bullhorn', 'eform-live' )
		);

		// Add the Bullhorn related stuff
		$this->add_field( [
			'section' => $section_id,
			'label' => __( 'Heading', 'eform-live' ),
			'settings' => 'sp_bullhorn_title',
			'type' => 'text',
		] );
		$this->add_field( [
			'section' => $section_id,
			'label' => __( 'Description', 'eform-live' ),
			'settings' => 'sp_bullhorn_desc',
			'type' => 'textarea',
		] );

		$ctas = [
			'one' => [
				__( 'CTA Button One Label', 'eform-live' ),
				__( 'CTA Button One Link', 'eform-live' ),
			],
			'two' => [
				__( 'CTA Button Two Label', 'eform-live' ),
				__( 'CTA Button Two Link', 'eform-live' ),
			],
		];

		foreach ( $ctas as $key => $cta ) {
			$this->add_field( [
				'section' => $section_id,
				'label' => $cta[0],
				'settings' => "sp_bullhorn_cta_{$key}_label",
				'type' => 'text',
			] );
			$this->add_field( [
				'section' => $section_id,
				'label' => $cta[1],
				'settings' => "sp_bullhorn_cta_{$key}_url",
				'type' => 'text',
			] );
		}
	}

	/**
	 * Register all FrontPage related settings and panels and controls.
	 *
	 * @return void
	 */
	public function register_frontpage_configs() {
		// Add the panel
		$panel_id = $this->register_panel_id( 'fp' );
		EForm_Live_Kirki::add_panel(
			$panel_id, [
				'priority' => 10,
				'title' => __( 'FrontPage Configuration', 'eform-live' ),
				'description' => __( 'Configure Different Sections of Frontpage', 'eform-live' ),
				'active_callback' => 'eform_live_is_frontpage',
			]
		);
		// Register all sections and controls
		$this->register_frontpage_hero( $panel_id );
		$this->register_frontpage_brands( $panel_id );
		$this->register_frontpage_steps( $panel_id );
		$this->register_frontpage_testimonial( $panel_id );
		$this->register_frontpage_videodemo( $panel_id );
		$this->register_frontpage_features( $panel_id );
		$this->register_frontpage_cards( $panel_id );
	}

	/**
	 * Register Kirki Config for FrontPage Hero section.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_hero( $panel_id ) {
		// Add a section
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'fp_hero',
			__( 'FrontPage Hero Section', 'eform-live' )
		);
		// Add controls
		// Slogan (tex)
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'hero_slogan',
				'label' => __( 'Slogan', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Subtitle (text)
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'hero_subtitle',
				'label' => __( 'Subtitle', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// CTA Primary
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'primary_cta_text',
				'label' => __( 'Primary CTA Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'link',
				'settings' => 'primary_cta_url',
				'label' => __( 'Primary CTA URL', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// CTA Secondary
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'secondary_cta_text',
				'label' => __( 'Secondary CTA Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'link',
				'settings' => 'secondary_cta_url',
				'label' => __( 'Secondary CTA URL', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Video
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'hero_video_cta_text',
				'label' => __( 'Video CTA Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'hero_video_cta_id',
				'label' => __( 'YouTube Video ID', 'eform-live' ),
				'section' => $section_id,
			]
		);
	}

	/**
	 * Register Kirki Config for FrontPage Brands Section.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_brands( $panel_id ) {
		// Add a new section
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'fp_intg',
			__( 'Compatible Services', 'eform-live' )
		);
		// Add brand cta and label
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'brand_cta_label',
				'label' => __( 'More Integrations Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'link',
				'settings' => 'brand_cta_link',
				'label' => __( 'More Integrations Link', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Add a repeater control
		$this->add_field(
			[
				'type' => 'repeater',
				'label' => __( 'Brand Logos', 'eform-live' ),
				'section' => $section_id,
				'row_label' => [
					'type' => 'text',
					'value' => __( 'Brand', 'eform-live' ),
				],
				'button_label' => __( 'New Brand', 'eform-live' ),
				'settings' => 'brands',
				'fields' => [
					'link_image' => [
						'type' => 'image',
						'label' => __( 'Brand Image', 'eform-live' ),
					],
					'link_label' => [
						'type' => 'text',
						'label' => __( 'Brand Name', 'eform-live' ),
					],
				],
			]
		);
	}

	/**
	 * Register Kirki Config for FrontPage Steps Section.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_steps( $panel_id ) {
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'fp_steps',
			__( 'Usage Steps', 'eform-live' )
		);

		// Heading
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'fp_steps_title',
				'label' => __( 'Section Title', 'eform-live' ),
				'section' => $section_id,
			]
		);

		// Step One
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'step_one_title',
				'label' => __( 'Step 1 Title', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'image',
				'settings' => 'step_one_image',
				'label' => __( 'Step 1 Image', 'eform-live' ),
				'section' => $section_id,
				'choices'     => array(
					'save_as' => 'id',
				),
			]
		);

		// Step Two
		foreach ( [
			2 => 'two',
			3 => 'three',
		] as $index => $step ) {
			$this->add_field(
				[
					'type' => 'text',
					'settings' => "step_{$step}_title",
					/* translators: %d is the numeric value of step (2, 3) */
					'label' => sprintf( __( 'Step %d Title', 'eform-live' ), $index ),
					'section' => $section_id,
				]
			);
			$this->add_field(
				[
					'type' => 'repeater',
					/* translators: %d is the numeric value of step (2, 3) */
					'label' => sprintf( __( 'Step %d Points', 'eform-live' ), $index ),
					'section' => $section_id,
					'row_label' => [
						'type' => 'text',
						'value' => __( 'Point', 'eform-live' ),
					],
					'button_label' => __( 'New Point', 'eform-live' ),
					'settings' => "step_{$step}_points",
					'fields' => [
						'step_svg' => [
							'type' => 'textarea',
							'label' => __( 'SVG Image', 'eform-live' ),
							'sanitize_callback' => function( $value ) {
								return $value;
							},
						],
						'step_url' => [
							'type' => 'text',
							'label' => __( 'Link', 'eform-live' ),
						],
						'step_label' => [
							'type' => 'text',
							'label' => __( 'Label', 'eform-live' ),
						],
					],
				]
			);
		}
	}

	/**
	 * Register Kirki Config for FrontPage Testimonials.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_testimonial( $panel_id ) {
		$section_id = $this->register_section_to_panel( $panel_id, 'fp_testi', __( 'Testimonial', 'eform-live' ) );

		// Add heading
		$this->add_field(
			[
				'type' => 'text',
				'label' => __( 'Section Heading', 'eform-live' ),
				'settings' => 'fp_testi_title',
				'section' => $section_id,
			]
		);

		// Add repeater testimonial
		$this->add_field(
			[
				'type' => 'repeater',
				/* translators: %d is the numeric value of step (2, 3) */
				'label' => __( 'Testimonials', 'eform-live' ),
				'section' => $section_id,
				'row_label' => [
					'type' => 'text',
					'value' => __( 'Testimonial', 'eform-live' ),
				],
				'button_label' => __( 'New Testimonial', 'eform-live' ),
				'settings' => 'testimonials',
				'fields' => [
					'text' => [
						'type' => 'textarea',
						'label' => __( 'Content', 'eform-live' ),
					],
					'link' => [
						'type' => 'text',
						'label' => __( 'Link', 'eform-live' ),
					],
					'name' => [
						'type' => 'text',
						'label' => __( 'Name', 'eform-live' ),
					],
					'star' => [
						'type' => 'number',
						'label' => __( 'Stars', 'eform-live' ),
						'choices' => [
							'min' => 1,
							'max' => 5,
							'step' => 1,
						],
					],
				],
			]
		);
	}

	/**
	 * Register Kirki Config for FrontPage Video Demo.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_videodemo( $panel_id ) {
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'fp_video',
			__( 'Video Section', 'eform-live' )
		);

		// Title
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'video_title',
				'label' => __( 'Section Title', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Video
		$this->add_field(
			[
				'type' => 'upload',
				'settings' => 'video_media',
				'label' => __( 'Upload Video', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'image',
				'settings' => 'video_poster',
				'label' => __( 'Upload Poster', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Subtitle
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'video_subtitle',
				'label' => __( 'Section Subtitle', 'eform-live' ),
				'section' => $section_id,
			]
		);
		// Cta
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'video_cta_text',
				'label' => __( 'CTA Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
		$this->add_field(
			[
				'type' => 'link',
				'settings' => 'video_cta_link',
				'label' => __( 'CTA URL', 'eform-live' ),
				'section' => $section_id,
			]
		);
	}

	/**
	 * Register Kirki Config for Frontpage Features.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_features( $panel_id ) {
		// Let's do nothing for now, instead create with gutenberg
	}

	/**
	 * Register Kirki Config for FrontPage Cards.
	 *
	 * @param string $panel_id The Id of the registered panel.
	 *
	 * @return void
	 */
	public function register_frontpage_cards( $panel_id ) {
		// This will pick from `cards` post-type
		// Add an option for how many to show
		$section_id = $this->register_section_to_panel(
			$panel_id,
			'fp_cards',
			__( 'Cards Section', 'eform-live' )
		);
		// Add number of items
		$this->add_field(
			[
				'type' => 'slider',
				'settings' => 'card_max_items',
				'label' => __( 'Initial Number of Cards', 'eform-live' ),
				'section' => $section_id,
				'choices' => [
					'min' => 2,
					'max' => 8,
					'step' => 1,
				],
			]
		);
		// Add cta text
		$this->add_field(
			[
				'type' => 'text',
				'settings' => 'card_cta_label',
				'label' => __( 'CTA Label', 'eform-live' ),
				'section' => $section_id,
			]
		);
	}
}
