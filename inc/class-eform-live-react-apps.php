<?php
/**
 * Class for all react-apps (shortcode) the theme provides.
 *
 * @package EFormLiveTheme
 */

/**
 * Primary react app class.
 * Holds all shortcodes.
 */
class EForm_Live_React_Apps {
	/**
	 * Enqueue var dependency.
	 *
	 * @var \WPackio\Enqueue
	 */
	private $enqueue;

	/**
	 * Create an instance
	 *
	 * @param \WPackio\Enqueue $enqueue Enqueue object.
	 */
	public function __construct( \WPackio\Enqueue $enqueue ) {
		$this->enqueue = $enqueue;
		// Add shortcodes
		add_shortcode( 'efl-forms', [ $this, 'form_app' ] );
		add_shortcode( 'efl-themes', [ $this, 'theme_app' ] );

		// Add admin ajax stuff
		add_action( 'wp_ajax_efl-download-form', array( $this, 'download_export' ) );
		add_action( 'wp_ajax_nopriv_efl-download-form', array( $this, 'download_export' ) );
	}

	/**
	 * Ajax callback to download a form.
	 */
	public function download_export() {
		global $ipt_fsqm_info, $wpdb;
		// Get form id
		$form_id = @$_GET['form_id'];
		// If not present
		if ( ! $form_id ) {
			die( __( 'Cheatin&#8217; uh?', 'eform-live' ) );
		}

		// Get form
		$form = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$ipt_fsqm_info['form_table']} WHERE id = %d", $form_id ), ARRAY_A ); // WPCS: unprepared SQL ok.

		// If form not present
		if ( null == $form ) {
			wp_die( __( 'Cheatin&#8217; uh?', 'eform-live' ) );
		}

		// Now change some settings
		// Emails
		$form['settings'] = maybe_unserialize( $form['settings'] );
		$form['settings']['user']['notification_email'] = '';
		$form['settings']['user']['notification_from'] = '';
		$form['settings']['admin']['email'] = '';
		$form['settings']['admin']['from_name'] = '';
		// Theme
		if ( isset( $_REQUEST['fsqm_theme'] ) ) {
			$form['settings']['theme']['template'] = strip_tags( $_REQUEST['fsqm_theme'] );
		}
		// Google Analytics
		$form['settings']['ganalytics'] = array(
			'enabled' => false,
			'manual_load' => false,
			'tracking_id' => '',
			'cookie' => 'auto',
		);
		// Social
		$form['settings']['social'] = array(
			'show' => false,
			'sites' => array( 'facebook_url' => true, 'twitter_url' => true, 'google_url' => true, 'pinterest_url' => true ),
			'image' => '',
			'facebook_app' => '',
			'url' => '%SELF%',
			'fb_url' => '',
			'title' => '%NAME%',
			'description' => 'I have scored %SCORE% in the quiz. Check yours now.',
			'twitter_via' => '',
			'twitter_hash' => 'quiz',
			'follow_on_social' => false,
			'auto_append_user' => false,
		);
		// Payments
		$form['settings']['payment']['paypal']['d_settings'] = array(
			'client_id' => '',
			'client_secret' => '',
		);
		$form['settings']['payment']['stripe']['api'] = '';
		// WooCommerce
		$form['settings']['payment']['woocommerce']['product_id'] = '';
		// Core
		$form['settings']['core']['post']['user_id'] = '';
		$form['settings']['core']['post']['post_type'] = 'post';
		// Serialize again
		$form['settings'] = maybe_serialize( $form['settings'] );

		// Now prepare the download
		$export = base64_encode( maybe_serialize( $form ) );
		$export = chunk_split( $export );

		// Pass header
		header( 'Content-Type: application/octet-stream' );
		header( 'Content-Disposition: attachment; filename=' . sanitize_file_name( 'eForm-' . $form['name'] . '.txt' ) );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate' );
		header( 'Pragma: public' );
		header( 'Content-Length: ' . mb_strlen( $export ) );
		echo $export;
		exit();
	}

	/**
	 * Enqueue shortcode assets depending on whether it is needed.
	 */
	public function enqueue() {
		// We would like to conditionally insert depending on
		// single post or page or any custom post type.
		if ( is_singular() && have_posts() ) {
			// Get the first post
			the_post();
			global $post;

			// Now if global $post is a proper class
			if ( is_a( $post, 'WP_Post' ) ) {
				// Enqueue assets for form app
				if ( has_shortcode( $post->post_content, 'efl-forms' ) ) {
					$this->enqueue->enqueue( 'react-apps', 'forms', [] );
				}

				// Enqueue assets for theme app
				if ( has_shortcode( $post->post_content, 'efl-themes' ) ) {
					$this->enqueue->enqueue( 'react-apps', 'themes', [] );
				}
			}
			rewind_posts();
		}
	}

	/**
	 * Shortcode callback for form application.
	 *
	 * @param array       $atts Shortcode attributes.
	 * @param string|null $content Shortcode attribute.
	 *
	 * @return string Shortcode content.
	 */
	public function form_app( $atts, $content = null ) {
		global $ipt_fsqm_info;
		/**
		 * Global WPDB.
		 *
		 * @var \wpdb
		 */
		global $wpdb;

		$atts = shortcode_atts(
			array(
				'categories' => '',
				'sticky' => '',
			), $atts, 'efl-forms'
		);

		// If no eForm
		if ( ! $ipt_fsqm_info ) {
			return __( 'eForm Not Present', 'eform-live' );
		}

		// Option
		$categories = $wpdb->get_col( "SELECT id FROM {$ipt_fsqm_info['category_table']}" ); // WPCS: unprepared SQL ok.

		// Override if provided by shortcode
		if ( '' != $atts['categories'] ) {
			$categories = wp_parse_id_list( $atts['categories'] );
		}

		// If no categories selected
		if ( empty( $categories ) ) {
			return __( 'No eForm Categories Selected.', 'eform-live' );
		}

		// Now fetch the form id and name
		$cat_id_vals = array_map( 'intval', $categories );
		$category = implode( ',', $cat_id_vals );
		$stickies = wp_parse_id_list( $atts['sticky'] );
		$query = "SELECT id, name, category FROM {$ipt_fsqm_info['form_table']} WHERE category IN ({$category})"; // WPCS: unprepared SQL ok.
		if ( ! empty( $stickies ) ) {
			$stickies = array_map( 'intval', $stickies );
			$query .= ' ORDER BY case WHEN id IN (' . implode( ',', $stickies ) . ') THEN 9999999999 ELSE id END DESC';
		} else {
			$query .= ' ORDER BY id DESC';
		}

		$forms = $wpdb->get_results( $query, ARRAY_A ); // WPCS: unprepared SQL ok.
		$cat_db_info = $wpdb->get_results( "SELECT id, name FROM {$ipt_fsqm_info['category_table']} WHERE id IN ({$category})" ); // WPCS: unprepared SQL ok.
		$cat_info = array();
		foreach ( $cat_db_info as $cat ) {
			$cat_info[] = [
				'id' => $cat->id,
				'name' => $cat->name,
			];
		}

		// Add link to the forms
		foreach ( $forms as $key => $form ) {
			$forms[ $key ]['url'] = IPT_FSQM_Form_Elements_Static::standalone_permalink_parts( $form['id'] )['url'];
		}

		$data = [
			'forms' => $forms,
			'cats' => $cat_info,
			'download_url' => admin_url( 'admin-ajax.php' ),
		];

		return '<div class="efl-live-form-examples"'
			. ' data-forms="' . esc_attr( json_encode( $data ) ) . '">'
			. $this->get_loader_dom()
			. '</div>';
	}

	/**
	 * Shortcode callback for the theme app.
	 *
	 * @param array       $atts Shortcode attributes.
	 * @param string|null $content Shortcode attribute.
	 *
	 * @return string Shortcode content.
	 */
	public function theme_app( $atts, $content = null ) {
		global $ipt_fsqm_info;

		$atts = shortcode_atts( array(
			'id' => '',
		), $atts, 'efl-forms' );

		// If no eForm
		if ( ! $ipt_fsqm_info ) {
			return __( 'eForm Not Present', 'eform-live' );
		}

		$form_url = IPT_FSQM_Form_Elements_Static::standalone_permalink_parts( $atts['id'] );
		if ( ! $form_url ) {
			return __( 'Invalid Form', 'eform-live' );
		}

		// Preset colors
		$colors = [
			[
				'code' => '#009688',
				'id' => 'default',
				'name' => __( 'Teal Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#607d8b',
				'id' => 'bg',
				'name' => __( 'Blue Grey Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#9c27b0',
				'id' => 'purple',
				'name' => __( 'Purple Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#673ab7',
				'id' => 'deep-purple',
				'name' => __( 'Deep Purple Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#3f51b5',
				'id' => 'indigo',
				'name' => __( 'Indigo Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#2196f3',
				'id' => 'blue',
				'name' => __( 'Blue Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#00bcd4',
				'id' => 'cyan',
				'name' => __( 'Cyan Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#ff5722',
				'id' => 'deep-orange',
				'name' => __( 'Deep Orange Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#795548',
				'id' => 'brown',
				'name' => __( 'Brown Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#9e9e9e',
				'id' => 'grey',
				'name' => __( 'Grey Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#4caf50',
				'id' => 'green',
				'name' => __( 'Green Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#f44336',
				'id' => 'red',
				'name' => __( 'Red Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#e91e63',
				'id' => 'pink',
				'name' => __( 'Pink Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#03a9f4',
				'id' => 'light-blue',
				'name' => __( 'Light Blue Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#8bc34a',
				'id' => 'light-green',
				'name' => __( 'Light Green Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#cddc39',
				'id' => 'lime',
				'name' => __( 'Lime Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#ffeb3b',
				'id' => 'yellow',
				'name' => __( 'Yellow Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#ffc107',
				'id' => 'amber',
				'name' => __( 'Amber Color Scheme', 'eform-live' ),
			],
			[
				'code' => '#ff9800',
				'id' => 'orange',
				'name' => __( 'Orange Color Scheme', 'eform-live' ),
			],
		];

		// Preset styles
		$styles = [
			[
				'id' => 'boxy',
				'name' => __( 'Boxy', 'eform-live' ),
			],
			[
				'id' => 'material',
				'name' => __( 'Material', 'eform-live' ),
			],
		];

		// Preset variants
		$variants = [
			[
				'id' => 'light',
				'name' => __( 'Light', 'eform-live' ),
			],
			[
				'id' => 'dark',
				'name' => __( 'Dark', 'eform-live' ),
			],
		];

		$data = [
			'url' => $form_url,
			'colors' => $colors,
			'styles' => $styles,
			'variants' => $variants,
		];

		return '<div class="efl-live-form-themes"'
			. ' data-themes="' . esc_attr( json_encode( $data ) ) . '">'
			. $this->get_loader_dom()
			. '</div>';
	}

	/**
	 * Get DOM for the loader.
	 */
	private function get_loader_dom() {
		return '<div class="efl-dl">'
			. '<div class="efl-dl__wrap">'
			. '<div class="efl-dl__container">'
			. '<div class="efl-dl__corner efl-dl__corner--top"></div>'
			. '<div class="efl-dl__corner efl-dl__corner--bottom"></div>'
			. '</div>' // .efl-dl__container
			. '<div class="efl-dl__square"></div>'
			. '</div>' // .efl-dl__wrap
			. '</div>'; // .efl-dl
	}
}
