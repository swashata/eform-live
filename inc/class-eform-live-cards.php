<?php
/**
 * Holds EForm_Live_Cards class.
 *
 * @package EFormLiveTheme
 */

/**
 * Handle Cards related post type and meta boxes
 */
class EForm_Live_Cards {

	/**
	 * Create an instance and add actions to different
	 * hooks to make cards work in the admin.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
		add_action( 'save_post', [ $this, 'save_post' ], 10, 2 );
	}

	/**
	 * Register post type and associated controls.
	 */
	public function register() {
		register_post_type( 'efl_cards', [
			'labels' => [
				'name' => __( 'Cards', 'eform-live' ),
				'singular_name' => __( 'Card', 'eform-live' ),
				'add_new_item' => __( 'Add New Card', 'eform-live' ),
			],
			'exclude_from_search' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_icon' => 'dashicons-nametag',
			'supports' => [
				'title',
				'excerpt',
				'page-attributes',
			],
			'register_meta_box_cb' => [ $this, 'meta_box_cb' ],
		] );
	}

	/**
	 * Register metaboxes for the cards post type.
	 */
	public function meta_box_cb() {
		// Add metabox
		add_meta_box(
			'efl-cards-svg',
			__( 'Card Icon (SVG)', 'eform-live' ),
			[ $this, 'metabox_svg_form' ]
		);
	}

	/**
	 * Create the form for metabox SVG form.
	 *
	 * @global WP_Post $post
	 */
	public function metabox_svg_form() {
		/**
		 * Global post object.
		 *
		 * @var \WP_Post
		 */
		global $post;

		$svg = get_post_meta( $post->ID, 'eflsvg', true );

		wp_nonce_field( 'save_efl_svg', 'efl_svg_nonce' );
		echo '<label class="screen-reader-text" for="eflsvg">SVG Code</label>';
		echo '<textarea class="widefat code" rows="10" cols="40" name="eflsvg" id="eflsvg">'
			. esc_textarea( $svg )
			. '</textarea>';
		echo '<p>'
			. __( 'Please enter RAW SVG Code here.', 'eform-live' )
			. '</p>';
	}

	/**
	 * Hook into all save_post and specifically save ours.
	 *
	 * @param int     $post_id The ID of the current post.
	 * @param WP_Post $post The post object.
	 *
	 * @return void.
	 */
	public function save_post( $post_id, $post ) {
		// Return if the user doesn't have edit permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		// Verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times.
		if ( ! isset( $_POST['eflsvg'] )
		|| ! wp_verify_nonce( $_POST['efl_svg_nonce'], 'save_efl_svg' )
		) {
			return;
		}
		// Now that we're authenticated, time to save the data.
		// This sanitizes the data from the field and saves it into an array $events_meta.
		$events_meta['eflsvg'] = wp_unslash( $_POST['eflsvg'] );
		// Cycle through the $events_meta array.
		// Note, in this example we just have one item, but this is helpful if you have multiple.
		foreach ( $events_meta as $key => $value ) {
			// Don't store custom data twice
			if ( 'revision' === $post->post_type ) {
				return;
			}
			if ( get_post_meta( $post_id, $key, true ) ) {
				// If the custom field already has a value, update it.
				update_post_meta( $post_id, $key, $value );
			} else {
				// If the custom field doesn't have a value, add it.
				add_post_meta( $post_id, $key, $value );
			}
			if ( ! $value ) {
				// Delete the meta key if there's no value
				delete_post_meta( $post_id, $key );
			}
		}
	}
}
