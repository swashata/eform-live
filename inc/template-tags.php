<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package eForm_Live_Preview
 */

if ( ! function_exists( 'eform_live_get_entry_date' ) ) :

	/**
	 * Get Entry date for 'post's (post type === 'post').
	 * If any other post type is found, it returns empty string.
	 *
	 * @return string Formatted post date.
	 */
	function eform_live_get_entry_date() {
		if ( 'post' !== get_post_type() ) {
			return '';
		}

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated sr-only" datetime="%3$s">%4$s</time>';
		}

		$date_string = '<span class="month">' . get_the_date( 'M' ) . '</span> '
			. '<span class="day">' . get_the_date( 'd' ) . '</span>, '
			. '<span class="year">' . get_the_date( 'Y' ) . '</span>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( 'c' ) ),
			$date_string,
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		return $time_string;
	}
endif;

if ( ! function_exists( 'eform_live_post_metabox' ) ) :

	/**
	 * Get meta box for post with authors, posted in and
	 * posted on.
	 *
	 * Must be called in a loop.
	 *
	 * @return void.
	 */
	function eform_live_post_metabox() {
		if ( 'post' !== get_post_type() ) {
			return;
		}


		echo '<div class="efl-page-post-metabox">';
		echo '<div class="efl-page-post-metabox__author">';
		echo '<img src="'
			. get_avatar_url( get_the_author_meta( 'ID' ), [
				'size' => 96,
			] )
			. '" alt="' . esc_attr( get_the_author() ) . '"'
			. ' class="efl-page-post-metabox__avatar" />';
		echo '<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '"'
			. ' class="efl-page-post-metabox__authorlink">'
			. get_the_author()
			. '</a>';
		echo '</div>'; // .efl-page-post-metabox__author

		$metas = [
			eform_live_get_entry_date(),
		];
		$categories_list = get_the_category_list( esc_html__( ', ', 'eform-live' ) );
		if ( $categories_list ) {
			/* translators: %s is list of categories */
			$metas[] = sprintf( __( 'in %s', 'eform-live' ), $categories_list );
		}
		echo '<div class="efl-page-post-metabox__entrymeta">'
			. implode( ', ', $metas )
			. '</div>';

		$link = get_the_permalink();
		$fb = add_query_arg( 'u', $link, 'https://www.facebook.com/sharer/sharer.php' );
		$tw = add_query_arg(
			[
				'url' => $link,
				'text' => get_the_title(),
			],
			'https://twitter.com/share'
		);

		echo '<div class="efl-page-post-metabox__share">'
			. '<a class="efl-page-post-metabox__sharebtn" href="' . $tw . '">' . eform_live_get_svg_icon( 'metabox/twitter.svg' ) . '</a>'
			. '<a class="efl-page-post-metabox__sharebtn" href="' . $fb . '">' . eform_live_get_svg_icon( 'metabox/facebook.svg' ) . '</a>'
			. '</div>';

		echo '</div>'; // .efl-page-post-metabox
	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function eform_live_categorized_blog() {
	$all_the_cool_cats = get_transient( 'eform_live_categories' );
	if ( false === $all_the_cool_cats ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories(
			array(
				'fields'     => 'ids',
				'hide_empty' => 1,
				// We only need to know if there is more than one category.
				'number'     => 2,
			)
		);

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'eform_live_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so eform_live_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so eform_live_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in eform_live_categorized_blog.
 */
function eform_live_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'eform_live_categories' );
}
add_action( 'edit_category', 'eform_live_category_transient_flusher' );
add_action( 'save_post', 'eform_live_category_transient_flusher' );


if ( ! function_exists( 'eform_live_breadcrumb' ) ) :
	/**
	 * A custom breadcrumb for Theme.
	 *
	 * For singular posts and pages, it should be called within the loop.
	 */
	function eform_live_breadcrumb() {
		global $post;
		?>
	<ol id="breadcrumbs" class="breadcrumb efl-breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/">
		<?php if ( is_front_page() ) : ?>
		<li class="active" typeof="v:Breadcrumb">
			<span property="v:title"><?php _e( 'Home', 'eform-live' ); ?></span>
		</li>
		<?php else : ?>
		<li typeof="v:Breadcrumb">
			<a rel="v:url" property="v:title" href="<?php echo home_url( '/' ); ?>"><?php _e( 'Home', 'eform-live' ); ?></a>
		</li>
		<?php endif; ?>

		<?php if ( is_attachment() ) : ?>
			<?php $parent_id = $post->post_parent; ?>
			<?php if ( ! empty( $parent_id ) ) : ?>
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?php echo esc_attr( get_permalink( $parent_id ) ); ?>"><?php echo get_the_title( $parent_id ); ?></a>
				</li>
			<?php endif; ?>
			<li class="active" typeof="v:Breadcrumb">
				<span property="v:title"><?php the_title(); ?></span>
			</li>
			<?php elseif ( is_singular( 'post' ) ) : ?>
			<?php $category = get_the_category(); ?>
			<?php $clink = get_category_link( $category[0]->term_id ); ?>
			<?php if ( $category[0]->parent != '0' ) : ?>
				<?php $cat_stack = array(); ?>
				<?php $pcat_id = $category[0]->parent; ?>
				<?php
				do {
					$pcat = get_category( $pcat_id );
					$cat_stack[] = array(
						'title' => $pcat->name,
						'link' => get_category_link( $pcat->term_id ),
					);
					$pcat_id = $pcat->parent;
				} while ( $pcat_id != '0' );
				?>
				<?php while ( $parent_cat = array_pop( $cat_stack ) ) : ?>
					<li typeof="v:Breadcrumb">
						<a rel="v:url" property="v:title" href="<?php echo esc_attr( $parent_cat['link'] ); ?>"><?php echo $parent_cat['title']; ?></a>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
			<li typeof="v:Breadcrumb">
				<a rel="v:url" property="v:title" href="<?php echo esc_attr( $clink ); ?>"><?php echo $category[0]->name; ?></a>
			</li>
			<li class="active" typeof="v:Breadcrumb">
				<a rel="v:url" property="v:title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</li>
			<?php elseif ( is_page() && ! is_front_page() ) : ?>
			<?php
			$parent_id = $post->post_parent;
			$parent_pages = array();
			while ( $parent_id ) {
				$parent_pages[] = get_page( $parent_id );
				$parent_id = $parent_pages[ count( $parent_pages ) - 1 ]->post_parent;
			}
			$parent_pages = array_reverse( $parent_pages );
			?>
			<?php if ( ! empty( $parent_pages ) ) : ?>
				<?php foreach ( $parent_pages as $parent ) : ?>
			<li typeof="v:Breadcrumb">
				<a rel="v:url" property="v:title" href="<?php echo get_permalink( $parent->ID ); ?>"><?php echo get_the_title( $parent->ID ); ?></a>
			</li>
			<?php endforeach; ?>
			<?php endif; ?>
			<li typeof="v:Breadcrumb" class="active">
				<a rel="v:url" property="v:title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</li>
		<?php elseif ( is_category() ) : ?>
			<?php $cat_id = get_query_var( 'cat' ); ?>
			<?php $cat = get_category( $cat_id ); ?>
			<?php if ( $cat->parent != '0' ) : ?>
				<?php $cat_stack = array(); ?>
				<?php $pcat_id = $cat->parent; ?>
				<?php
				do {
					$pcat = get_category( $pcat_id );
					$cat_stack[] = array(
						'title' => $pcat->name,
						'link' => get_category_link( $pcat->term_id ),
					);
					$pcat_id = $pcat->parent;
				} while ( $pcat_id != '0' );
				?>
				<?php while ( $parent_cat = array_pop( $cat_stack ) ) : ?>
					<li typeof="v:Breadcrumb">
						<a rel="v:url" property="v:title" href="<?php echo esc_attr( $parent_cat['link'] ); ?>"><?php echo $parent_cat['title']; ?></a>
					</li>
				<?php endwhile; ?>
			<?php endif; ?>
			<li typeof="v:Breadcrumb" class="active">
				<span property="v:title"><?php single_cat_title(); ?></span>
			</li>
			<?php elseif ( is_tag() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php single_tag_title(); ?></span>
				</li>
			<?php elseif ( is_tax() ) : ?>
				<?php
				global $wp_query;
				$term = $wp_query->get_queried_object();
				?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php echo $term->name; ?></span>
				</li>
			<?php elseif ( is_author() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php echo get_the_author(); ?></span>
				</li>
			<?php elseif ( is_day() || is_date() || is_month() || is_year() || is_time() ) : ?>
				<?php if ( is_year() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php echo get_the_date( 'Y' ); ?></span>
				</li>
				<?php elseif ( is_month() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php echo get_the_date( 'F Y' ); ?></span>
				</li>
				<?php elseif ( is_day() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<li property="v:title"><?php echo get_the_date(); ?></span>
				</li>
				<?php else : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php echo get_the_time(); ?></span>
				</li>
				<?php endif; ?>
			<?php elseif ( is_search() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php _e( 'Search result', 'eform-live' ); ?></span>
				</li>
			<?php elseif ( is_404() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php _e( 'Error 404 - Not found', 'eform-live' ); ?></span>
				</li>
			<?php elseif ( ! is_home() && ! is_front_page() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php _e( 'Archive', 'eform-live' ); ?></span>
				</li>
			<?php elseif ( is_home() && ! is_front_page() ) : ?>
				<li typeof="v:Breadcrumb" class="active">
					<span property="v:title"><?php _e( 'Blog', 'eform-live' ); ?></span>
				</li>
			<?php endif; ?>
	</ol>
		<?php
	}
endif;

if ( ! function_exists( 'eform_live_content_nav' ) ) :
	/**
	 * Display navigation to next/previous pages when applicable
	 *
	 * @param string $nav_id The HTML Id of the page navigation.
	 */
	function eform_live_content_nav( $nav_id ) {
		global $wp_query, $post;

		// Don't print empty markup on single pages if there's nowhere to navigate.
		if ( is_single() ) {
			$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
			$next = get_adjacent_post( false, '', false );

			if ( ! $next && ! $previous ) {
				return;
			}
		}

		// Don't print empty markup in archives if there's only one page.
		if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) ) {
			return;
		}

		$nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';

		// Add support with bootstrap pager
		$nav_class .= ' pager';

		?>
		<div class="efl-content">
			<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="efl-page-nav efl-page-nav--fluid <?php echo $nav_class; ?>">
			<span class="screen-reader-text"><?php _e( 'Post navigation', 'eform-live' ); ?></span>
			<?php
			if ( is_single() ) {
				echo str_replace(
					'<a',
					'<a class="efl-page-nav__prev"',
					get_previous_post_link( '%link', '&laquo; %title' )
				);
				echo str_replace(
					'<a',
					'<a class="efl-page-nav__next"',
					get_next_post_link( '%link', '%title &raquo;' )
				);
			} elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) {
				$next_posts_link = get_next_posts_link( __( '&laquo; Older', 'eform-live' ) );
				$prev_posts_link = get_previous_posts_link( __( 'Newer &raquo;', 'eform-live' ) );
				if ( $next_posts_link ) {
					echo str_replace( '<a', '<a class="efl-page-nav__prev"', $next_posts_link );
				}

				if ( $prev_posts_link ) {
					echo str_replace( '<a', '<a class="efl-page-nav__next"', $prev_posts_link );
				}
			}
			?>
			</nav>
		</div>
		<?php
	}
endif; // eform_live_content_nav

if ( ! function_exists( 'eform_live_site_logo' ) ) :
	/**
	 * Print out the header branding/logo for the Theme.
	 *
	 * It determines the proper tag to use for SEO.
	 *
	 * @param string $class_name Name of css class.
	 * @return void
	 */
	function eform_live_site_logo( $class_name = 'site-branding efl-site-header__logo' ) {
		?>
	<div class="<?php echo esc_attr( $class_name ); ?>">
		<?php
		$description = get_bloginfo( 'description', 'display' );
		$tag = 'div';
		if ( is_front_page() ) {
			$tag = 'h1';
		}
		?>
		<<?php echo $tag; ?> class="site-title" title="<?php echo esc_attr( $description ); ?>">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php if ( get_header_image() ) : ?>
					<img src="<?php header_image(); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="site-logo" />
				<?php endif; ?>
			</a>
		</<?php echo $tag; ?>>
	</div><!-- .site-branding -->
		<?php
	}
endif; // eform_live_site_branding()

if ( ! function_exists( 'eform_live_frontpage_hero' ) ) :

	/**
	 * Print the front page hero section.
	 *
	 * @return void.
	 */
	function eform_live_frontpage_hero() {
		$mods = eform_live_get_theme_mods( [
			'hero_slogan',
			'hero_subtitle',
			'primary_cta_text',
			'primary_cta_url',
			'secondary_cta_text',
			'secondary_cta_url',
			'hero_video_cta_text',
			'hero_video_cta_id',
		] );

		echo '<section class="efl-fp-hero">';

		echo '<div class="efl-fp-hero__background-svg">';
		echo '<div class="efl-fp-hero__background-svg-canvas">';

		foreach ( [ 'one', 'two', 'three' ] as $svg_num ) {
			echo '<div class="efl-fp-hero__svg-' . $svg_num . '">'
				. eform_live_get_svg( 'hero/form-' . $svg_num . '.svg' )
				. '</div>';
		}

		echo '</div>'; // .efl-fp-hero__background-svg-canvas
		echo '</div>'; // .efl-fp-hero__background-svg

		echo '<div class="efl-fp-hero__screen-backdrop">';

		echo '<h1 class="efl-fp-hero__title">'
			. eform_live_format_psuedo_md( $mods['hero_slogan'] )
			. '</h1>';
		echo '<h3 class="efl-fp-hero__subtitle">'
			. eform_live_format_psuedo_md( $mods['hero_subtitle'] )
			. '</h3>';
		echo '<div class="efl-fp-hero__ctas efl-cta-btn-group efl-cta-btn-group--align-center">';
		echo '<a href="'
			. esc_url( $mods['primary_cta_url'] )
			. '" class="efl-fp-hero__cta efl-cta-btn efl-cta-btn--white">'
			. $mods['primary_cta_text']
			. '</a>';
		echo '<a href="'
			. esc_url( $mods['secondary_cta_url'] )
			. '" class="efl-fp-hero__cta efl-cta-btn efl-cta-btn--solid">'
			. $mods['secondary_cta_text']
			. '</a>';
		echo '</div>'; // .efl-fp-hero__ctas
		echo '<div class="efl-fp-hero__video">';
		echo '<a href="#" class="efl-fp-hero__video-anchor">';
		echo  eform_live_get_svg_icon( 'playbutton.svg' );
		echo '<span>' . $mods['hero_video_cta_text'] . '</span>';
		echo '</a>'; // .efl-fp-hero__video-anchor
		echo '</div>'; // .efl-fp-hero__video
		echo '<div class="efl-fp-hero__video-screen">';
		echo '<a href="#" class="efl-fp-hero__video-close">'
			. eform_live_get_svg_icon( 'times.svg' )
			. '</a>';
		echo '<div class="efl-fp-hero__video-frame"'
			. ' data-id="' . esc_attr( $mods['hero_video_cta_id'] ) . '"'
			. '></div>';
		echo '</div>'; // .efl-fp-hero__video-screen

		echo '</div>'; // .efl-fp-hero__screen-backdrop
		echo '</section>'; // .efl-fp-hero
	}

endif; // eform_live_frontpage_hero

if ( ! function_exists( 'eform_live_frontpage_brands' ) ) :
	/**
	 * Print the brands for the front page.
	 */
	function eform_live_frontpage_brands() {
		$brands = (array) eform_live_get_theme_mod( 'brands' );
		$brand_cta = eform_live_get_theme_mods( [
			'brand_cta_link',
			'brand_cta_label',
		] );

		if ( ! count( $brands ) ) {
			return;
		}

		echo '<section class="efl-fp-brands">';
		echo '<div class="efl-fp-brands__items">';
		foreach ( $brands as $brand ) {
			echo '<div class="efl-fp-brands__item">';
			$image = wp_get_attachment_image( $brand['link_image'], 'full', false, [
				'alt' => $brand['link_label'],
				'class' => 'efl-fp-brands__image',
			] );
			echo $image;
			echo '</div>'; // .efl-fp-brands__item
		}
		echo '</div>'; // .efl-fp-brands__items

		if ( $brand_cta['brand_cta_link'] && $brand_cta['brand_cta_label'] ) {
			echo '<div class="efl-fp-brands__ctas">';

			echo '<div class="efl-fp-brands__btngroup efl-cta-btn-group efl-cta-btn-group--align-center">';
			echo '<a href="'
				. esc_url( $brand_cta['brand_cta_link'] )
				. '" class="efl-fp-brands__cta efl-cta-btn efl-cta-btn--solid efl-cta-btn--fluid efl-cta-btn--small">'
				. $brand_cta['brand_cta_label']
				. '</a>';
			echo '</div>'; // .efl-fp-brands__btngroup

			echo '</div>'; // .efl-fp-brands__ctas
		}

		echo '</section>'; // .efl-fp-brands
	}
endif;

if ( ! function_exists( 'eform_live_frontpage_steps' ) ) :

	/**
	 * Print the steps for the frontpage.
	 */
	function eform_live_frontpage_steps() {
		$mods = eform_live_get_theme_mods( [
			'fp_steps_title',
			'step_one_title',
			'step_one_image',
			'step_two_title',
			'step_two_points',
			'step_three_title',
			'step_three_points',
			'step_cta_label',
			'step_cta_link',
		] );

		echo '<section class="efl-fp-steps efl-section">';
		// Heading
		echo '<h2 class="efl-fp-steps__title efl-heading">'
			. eform_live_format_psuedo_md( $mods['fp_steps_title'] )
			. '</h2>'; // .efl-fp-steps__title

		echo '<div class="efl-fp-steps__group">';
		// Step one
		echo '<div class="efl-fp-steps__step efl-fp-steps__step--one">';
		echo '<h3 class="efl-fp-steps__steptitle">'
			. '<span class="efl-fp-steps__badge">1</span>'
			. '<span>' . eform_live_format_psuedo_md( $mods['step_one_title'] ) . '</span>'
			. '</h3>';
		echo '<div class="efl-fp-steps__content">';
		echo '<div class="efl-fp-steps__bgsvg">'
			. eform_live_get_svg( 'steps/one.svg' )
			. '</div>';
		echo wp_get_attachment_image( $mods['step_one_image'], 'full', false, [
			'class' => 'efl-fp-steps__image',
		] );
		echo '</div>'; // .efl-fp-steps__content
		echo '</div>'; // .efl-fp-steps__step--one

		// Step two and three
		foreach (
			[
				2 => 'two',
				3 => 'three',
			] as $key => $step
		) {
			echo '<div class="efl-fp-steps__step efl-fp-steps__step--' . $step . '">';
			echo '<h3 class="efl-fp-steps__steptitle">'
				. '<span class="efl-fp-steps__badge">' . $key . '</span>'
				. '<span>' . eform_live_format_psuedo_md( $mods[ 'step_' . $step . '_title' ] ) . '</span>'
				. '</h3>';
			echo '<div class="efl-fp-steps__content">';
			echo '<div class="efl-fp-steps__bgsvg">'
				. eform_live_get_svg( 'steps/' . $step . '.svg' )
				. '</div>';
			echo '<ul class="efl-fp-steps__list efl-fp-steps__tiltable">';
			foreach ( $mods[ 'step_' . $step . '_points' ] as $point ) {
				echo '<li>';
				echo '<a href="' . esc_attr( $point['step_url'] ) . '">'
					. eform_live_get_svg_icon( $point['step_svg'] )
					. '<span>'
					. eform_live_format_psuedo_md( $point['step_label'] )
					. '</span>'
					. '</a>';
				if ( $key === 2 ) {
					echo '<span class="efl-fp-steps__checkmark">'
						. eform_live_get_svg_icon( 'checkmark-with-circle.svg' )
						. '</span>';
				}
				echo '</li>';
			}
			echo '</ul>'; // .efl-fp-steps__list
			echo '</div>'; // .efl-fp-steps__content
			echo '</div>'; // .efl-fp-steps__step--{{$step}}
		}
		echo '</div>'; // .efl-fp-steps__group

		echo '<div class="efl-fp-steps__ctas efl-cta-btn-group efl-cta-btn-group--align-center">';
		echo '<a href="' . esc_url( $mods['step_cta_link'] )
			. '" class="efl-cta-btn efl-cta-btn--solid">'
			. $mods['step_cta_label']
			. '</a>';
		echo '</div>'; // .efl-fp-steps__ctas

		echo '</section>'; // .efl-fp-steps
	}
endif;

if ( ! function_exists( 'eform_live_frontpage_testimonials' ) ) :

	/**
	 * Print testimonial section to the frontpage.
	 */
	function eform_live_frontpage_testimonials() {
		$mods = eform_live_get_theme_mods( [
			'fp_testi_title',
			'testimonials',
		] );
		echo '<section class="efl-fp-testimonial efl-section">';
		echo '<div class="efl-container">';

		echo '<h2 class="efl-fp-testimonial__title efl-heading">'
			. eform_live_format_psuedo_md( $mods['fp_testi_title'] )
			. '</h2>';

		echo '<div class="efl-fp-testimonial__content">';
		echo '<a href="#" class="efl-fp-testimonial__nav-left">'
				. eform_live_get_svg_icon( 'testi/nav.svg' )
				. '</a>';
		echo '<a href="#" class="efl-fp-testimonial__nav-right">'
				. eform_live_get_svg_icon( 'testi/nav.svg' )
				. '</a>';
		echo '<div class="efl-fp-testimonial__siema">';
		foreach ( $mods['testimonials'] as $testimonial ) {
			echo '<div class="efl-fp-testimonial__item">';
			echo '<div class="efl-fp-testimonial__stars">';
			echo str_repeat( eform_live_get_svg_icon( 'testi/star.svg' ), (int) $testimonial['star'] );
			echo '</div>'; // .efl-fp-testimonial__stars
			echo '<div class="efl-fp-testimonial__para">';
			echo '<span class="efl-fp-testimonial__quote-left">'
				. eform_live_get_svg_icon( 'testi/quote.svg' )
				. '</span>';
			echo '<blockquote class="efl-fp-testimonial__blockquote">'
				. wpautop( eform_live_format_psuedo_md( $testimonial['text'] ) )
				. '</blockquote>';
			echo '<cite class="efl-fp-testimonial__cite">';
			printf(
				/* translators: %1$s is the author name */
				__( 'from: %1$s</span>', 'eform-live' ),
				'<span class="efl-fp-testimonial__authname">' . $testimonial['name'] . '</span>'
			);
			echo '</cite>'; // .efl-fp-testimonial__cite
			echo '<span class="efl-fp-testimonial__quote-right">'
				. eform_live_get_svg_icon( 'testi/quote.svg' )
				. '</span>';
			echo '</div>'; // .efl-fp-testimonial__para
			echo '</div>'; // .efl-fp-testimonial__item
		}
		echo '</div>'; // .efl-fp-testimonial__siema
		echo '<div class="efl-fp-testimonial__pager"></div>';
		echo '</div>'; // .efl-fp-testimonial__content

		echo '</div>'; // .efl-container
		echo '</section>'; // .efl-fp-testimonial
	}
endif;

if ( ! function_exists( 'eform_live_frontpage_livevideo' ) ) :
	/**
	 * Print the live video of the front page.
	 */
	function eform_live_frontpage_livevideo() {
		$mods = eform_live_get_theme_mods( [
			'video_title',
			'video_media',
			'video_poster',
			'video_subtitle',
			'video_cta_text',
			'video_cta_link',
		] );
		echo '<section class="efl-fp-video efl-section">';
		echo '<div class="efl-fp-video__bottom">'
			. eform_live_get_svg( 'demo/bottom-curve.svg' )
			. '</div>';

		echo '<div class="efl-container">';

		echo '<h2 class="efl-fp-video__title efl-heading">'
			. eform_live_format_psuedo_md( $mods['video_title'] )
			. '</h2>';

		echo '</div>'; // .efl-container

		echo '<div class="efl-fp-video__backdrop">';
		echo '<div class="efl-container">';

		echo '<div class="efl-fp-video__screen">';
		echo '<video class="efl-fp-video__embed" preload="auto" width="944" height="532" muted playsinline loop poster="' . $mods['video_poster'] . '">'
			. '<source src="' . $mods['video_media'] . '" type="video/mp4" />'
			. '</video>';
		echo '<a href="#" class="efl-fp-video__trigger">'
			. eform_live_get_svg_icon( 'playbutton.svg' )
			. '</a>';

		echo '</div>'; // .efl-fp-video__screen

		echo '<h3 class="efl-fp-video__subtitle efl-heading">'
			. eform_live_format_psuedo_md( $mods['video_subtitle'] )
			. '</h3>';

		echo '<div class="efl-fp-video__ctas efl-cta-btn-group efl-cta-btn-group--align-center">';
		echo '<a href="' . esc_url( $mods['video_cta_link'] )
			. '" class="efl-cta-btn efl-cta-btn--white">'
			. $mods['video_cta_text']
			. '</a>';
		echo '</div>'; // .efl-fp-video__ctas

		echo '</div>'; // .efl-container
		echo '</div>'; // .efl-fp-video__screen

		echo '</section>'; // .efl-fp-video
	}
endif;

if ( ! function_exists( 'eform_live_frontpage_cards' ) ) :
	/**
	 * Print cards on front page with more button.
	 */
	function eform_live_frontpage_cards() {
		$cards = new WP_Query( [
			'post_type' => 'efl_cards',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'orderby' => 'menu_order',
		] );
		$mods = eform_live_get_theme_mods( [
			'card_max_items',
			'card_cta_label',
		] );
		$max_items = absint( $mods['card_max_items'] );
		$current_item_index = 1;

		echo '<section class="efl-fp-cards efl-section">';
		echo '<div class="efl-container">';

		echo '<div class="efl-fp-cards__items">';
		if ( $cards->have_posts() ) {
			while ( $cards->have_posts() ) {
				$cards->the_post();
				$svg = get_post_meta( get_the_ID(), 'eflsvg', true );
				$class = 'efl-fp-cards__item';
				if ( $current_item_index > $max_items ) {
					$class .= ' efl-fp-cards__item--hidden';
				}
				echo '<div class="' . $class . '">';
				echo '<div class="efl-fp-cards__svg">'
					. $svg
					. '</div>';
				echo '<div class="efl-fp-cards__content">';
				echo '<h3 class="efl-fp-cards__title">'
					. get_the_title()
					. '</h3>';
				echo '<p class="efl-fp-cards__para">'
					. get_the_excerpt()
					. '</p>';
				echo '</div>'; // .efl-fp-cards__content
				echo '</div>'; // .efl-fp-cards__item
				$current_item_index++;
			}
		}
		wp_reset_postdata();
		echo '</div>'; // .efl-fp-cards__items

		if ( $current_item_index - 1 > $max_items ) {
			echo '<div class="efl-fp-cards__ctas efl-cta-btn-group efl-cta-btn-group--align-center">';
			echo '<a href="#" class="efl-fp-cards__more efl-cta-btn efl-cta-btn--outline">'
				. $mods['card_cta_label']
				. '</a>';
			echo '</div>'; // .efl-fp-video__ctas
		}

		echo '</div>'; // .efl-container
		echo '</section>'; // .efl-fp-cards
	}
endif;

if ( ! function_exists( 'eform_live_social_menu' ) ) :

	/**
	 * Print a social menu for eForm Live theme.
	 *
	 * This is used in the footer.
	 *
	 * @return void.
	 */
	function eform_live_social_menu() {
		$socials = eform_live_get_available_socials();
		echo '<ul class="efl-social-menu">';
		foreach ( $socials as $key => $value ) {
			$link = eform_live_get_theme_mod( "social_${key}" );
			if ( $link ) {
				echo '<li><a href="' . esc_attr( $link ) . '"'
					. ' target="_blank" rel="noopener noreferrer"'
					. '>'
					. eform_live_get_svg_icon( "social/{$key}.svg" )
					. '</a></li>';
			}
		}
		echo '</ul>'; // .efl-social-menu
	}
endif;

if ( ! function_exists( 'eform_live_get_page_title' ) ) :

	/**
	 * Get page title for any page.
	 *
	 * To be used in view templates of the theme.
	 *
	 * @return string Title of page.
	 */
	function eform_live_get_page_title() {
		$page_title = wp_title( '&dash;', false );

		if ( is_home() ) {
			/* translators: %s is blog name */
			$page_title = sprintf( __( '%s &dash; Blog', 'eform-live' ), get_bloginfo( 'name' ) );
		} elseif ( is_archive() ) {
			$page_title = get_the_archive_title();
		} elseif ( is_search() ) {
			/* translators: %s is search query */
			$page_title = sprintf( __( 'Showing results for <code>%s</code>', 'eform-live' ), get_search_query() );
		} elseif ( is_404() ) {
			$page_title = __( 'It\'s 404 and that\'s an error', 'eform-live' );
		}

		return $page_title;
	}
endif;

if ( ! function_exists( 'eform_live_page_heading' ) ) :

	/**
	 * Get Theme Page heading section with Title, breadcrumb etc.
	 *
	 * @param string  $page_title Heading.
	 * @param boolean $has_breadcrumb Whether to show breadcrumb.
	 * @param boolean $has_metabox Whether to show metabox.
	 * @param array   $modifiers Array of additional class modifiers.
	 *
	 * @return void.
	 */
	function eform_live_page_heading( $page_title, $has_breadcrumb = true, $has_metabox = false, $modifiers = [] ) {
		$classes[] = 'efl-page-heading efl-content';
		if ( ! empty( $modifiers ) ) {
			foreach ( $modifiers as $m ) {
				$classes[] = 'efl-content--' . $m;
			}
		}

		echo '<div class="' . esc_attr( implode( $classes, ' ' ) ) . '">'; // .efl-content

		echo '<h2 class="efl-page-title">' . $page_title . '</h2>';

		if ( $has_breadcrumb ) {
			eform_live_breadcrumb();
		}

		if ( $has_metabox ) {
			eform_live_post_metabox();
		}

		echo '</div>'; // .efl-content
	}
endif;

if ( ! function_exists( 'eform_live_bullhorn' ) ) :

	/**
	 * Print the bullhorn on pages.
	 *
	 * @return void.
	 */
	function eform_live_bullhorn() {
		$mods = eform_live_get_theme_mods( [
			'sp_bullhorn_title',
			'sp_bullhorn_desc',
			'sp_bullhorn_cta_one_label',
			'sp_bullhorn_cta_one_url',
			'sp_bullhorn_cta_two_label',
			'sp_bullhorn_cta_two_url',
		] );

		echo '<div class="efl-bullhorn">';
		echo '<div class="efl-bullhorn__content efl-content">';

		echo '<h2 class="efl-page-title efl-bullhorn__title">'
			. $mods['sp_bullhorn_title']
			. '</h2>';

		echo '<p class="efl-bullhorn__desc">'
			. eform_live_format_psuedo_md( $mods['sp_bullhorn_desc'] )
			. '</p>';

		echo '<div class="efl-bullhorn__ctas">';
		foreach ( [ 'one', 'two' ] as $key ) {
			echo '<a class="efl-bullhorn__cta"'
				. ' href="' . esc_attr( $mods[ "sp_bullhorn_cta_{$key}_url" ] ) . '">'
				. $mods[ "sp_bullhorn_cta_{$key}_label" ] . ' &rsaquo;'
				. '</a>';
		}
		echo '</div>'; // .efl-bullhorn__ctas

		echo '</div>'; // .efl-bullhorn__content
		echo '</div>'; // .efl-bullhorn
	}
endif;
