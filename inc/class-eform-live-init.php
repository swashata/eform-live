<?php
/**
 * Class for e form live initialize.
 *
 * It handles all functionalities of theme support and enqueue
 *
 * It is a singleton class
 *
 * @package EFormLiveTheme
 */

/**
 * EForm Live Init class.
 */
class EForm_Live_Init {
	/**
	 * Instance variable
	 *
	 * Stores the only instance
	 *
	 * @var EForm_Live_Init
	 */
	private static $instance = null;

	const VERSION = '5.0.0';

	/**
	 * Get the instance of the init class
	 *
	 * It properly instantiates the class and returns
	 * the instance object
	 *
	 * @return eForm_Live_Init object
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Enqueue instance from wpackio/enqueue
	 *
	 * @var WPackio\Enqueue
	 */
	private $enqueue;

	/**
	 * React App Shortcode instance.
	 *
	 * @var EForm_Live_React_Apps
	 */
	private $react_apps;

	/**
	 * Create an instance.
	 */
	private function __construct() {
		// Init the enqueue
		$this->enqueue = new \WPackio\Enqueue( 'eformLive', 'dist', self::VERSION, 'theme', false );

		// Set the content width
		add_action( 'after_setup_theme', array( $this, 'content_width' ), 0 );

		// After theme setup common stuff
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );

		// Enqueue
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
		// Late Enqueue
		add_action( 'get_footer', [ $this, 'enqueue_footer' ] );

		// Widgets
		add_action( 'widgets_init', array( $this, 'widgets_init' ) );

		// Init the react app (shortcodes)
		$this->react_apps = new EForm_Live_React_Apps( $this->enqueue );

		// Add gutenberg block
		add_action( 'init', [ $this, 'register_gutenblocks' ] );

		// Register custom post type for front-page
		$cards = new EForm_Live_Cards();
	}

	/**
	 * Do stuff in after theme setup hook
	 *
	 * It loads the text domain, add supports for a number of theme features
	 *
	 * Registers navigation menus etc
	 */
	public function after_setup_theme() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on eForm Live Preview, use a find and replace
		 * to change 'eform-live' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'eform-live', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for partial refresh for widget areas
		 *
		 * @link {https://make.wordpress.org/core/2016/03/22/implementing-selective-refresh-support-for-widgets/}
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add feature image size
		 *
		 * @link {https://developer.wordpress.org/reference/functions/add_image_size/}
		 */
		add_image_size( 'eform-live-feature', 750, 300, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'eform-live' ),
				'footer' => esc_html__( 'Footer', 'eform-live' ),
				'bottom' => esc_html__( 'Bottom Nav', 'eform-live' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Add custom header support
		add_theme_support(
			'custom-header', apply_filters(
				'eform_live_custom_header_args', array(
					'default-image'          => get_template_directory_uri() . '/images/logo.png',
					'default-text-color'     => 'BD6778',
					'header-text'            => false,
					'width'                  => 388,
					'height'                 => 144,
					'flex-height'            => true,
					'flex-width'             => true,
					'wp-head-callback'       => [ $this, 'eform_live_header_style' ],
				)
			)
		);

		/**
		 * Add support for different gutenberg features
		 * Since many of the building blocks are basically
		 * gutenberg blocks, we need to have as many features
		 * as possible.
		 */
		// We support align-wide
		add_theme_support( 'align-wide' );
		// We have custom color palette
		add_theme_support( 'editor-color-palette', array(
			array(
				'name' => __( 'Primary Color', 'eform-live' ),
				'slug' => 'efl-primary',
				'color' => '#e81e61',
			),
			array(
				'name' => __( 'Dark Blue', 'eform-live' ),
				'slug' => 'efl-blue-dark',
				'color' => '#1D2235',
			),
			array(
				'name' => __( 'Light Blue', 'eform-live' ),
				'slug' => 'efl-blue-light',
				'color' => '#516193',
			),
		) );
		// We support editor styles
		// add_theme_support( 'editor-styles' );
		// add_editor_style( 'editor-style.css' );
		// But we actually do the styling ourselves
		// so we don't need wp-block-styles
	}

	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see eform_live_custom_header_setup().
	 */
	public function eform_live_header_style() {
		$header_text_color = get_header_textcolor();

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: HEADER_TEXTCOLOR.
		 */
		if ( HEADER_TEXTCOLOR === $header_text_color ) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
		?>
	<style type="text/css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
			?>
		.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px, 1px, 1px, 1px);
		}
			<?php
			// If the user has set a custom color for the text use that.
			else :
				?>
		.site-title a,
		.site-description {
			color: #<?php echo esc_attr( $header_text_color ); ?>;
		}
	<?php endif; ?>
	/**/
	</style>
		<?php
	}

	/**
	 * Set the content width in pixels, based on the theme's design and
	 * stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global     int   $content_width
	 */
	public function content_width() {
		// $width = 780;
		// if ( is_page() ) {
		// $width = 1140;
		// }
		$GLOBALS['content_width'] = apply_filters( 'eform_live_content_width', 700 );
	}

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		// $version = self::VERSION;
		// Styles
		// Will be managed by wpk-scripts package
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Enqueue front-page assets
		if ( eform_live_is_frontpage() ) {
			$this->enqueue->enqueue( 'front', 'main', [] );
		} else {
			$this->enqueue->enqueue( 'site', 'main', [] );
			$this->react_apps->enqueue();
		}
	}

	/**
	 * Register gutenberg blocks that comes with the theme.
	 */
	public function register_gutenblocks() {
		if ( function_exists( 'register_block_type' ) ) {
			// Get assets for enqueue on gutenberg
			$gutenberg_assets = $this->enqueue->register( 'gutenberg', 'main', [
				'js_deps' => [
					'wp-components',
					'wp-element',
					'wp-blocks',
					'wp-i18n',
					'wp-editor',
				],
				'in_footer' => false,
			] );
			// Enqueue all js
			$jses = is_array( $gutenberg_assets['js'] )
				? $gutenberg_assets['js']
				: [];
			$csses = is_array( $gutenberg_assets['css'] )
				? $gutenberg_assets['css']
				: [];

			$block_deps = [];
			if ( count( $jses ) ) {
				$block_deps['editor_script'] = array_pop( $jses )['handle'];
			}
			if ( count( $csses ) ) {
				$block_deps['editor_style'] = array_pop( $csses )['handle'];
			}
			register_block_type( 'eform-live-theme/feature', $block_deps );
		}
	}

	/**
	 * Enqueue some delegated scripts and styles specifically in the footer.
	 *
	 * @return void
	 */
	public function enqueue_footer() {
		$version = self::VERSION;
		if ( ! eform_live_is_frontpage() ) {
			wp_enqueue_style( 'ipt-icomoon', get_template_directory_uri() . '/fonts/icomoon.css', array(), $version );
		}
	}

	/**
	 * Adds widgets area to sidebar and footer
	 */
	public function widgets_init() {
		// No sidebar
	}
}
