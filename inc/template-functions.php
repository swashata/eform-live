<?php
/**
 * Additional functions to add styling to the template
 *
 * @package eForm_Live_Preview
 * @subpackage Template_Functions
 */

/**
 * Checks to see if we're on the homepage or not.
 *
 * @return boolean True if on front-page.
 */
function eform_live_is_frontpage() {
	return ( is_front_page() && ! is_home() );
}

/**
 * Checks to see if we are not on the frontpage.
 *
 * @return boolean True if not on front-page.
 */
function eform_live_is_not_frontpage() {
	return ! eform_live_is_frontpage();
}

/**
 * Get a theme mod registered by eform-live theme, while
 * considering the default.
 *
 * @param string $mod The mod key.
 * @return mixed The theme mod.
 */
function eform_live_get_theme_mod( $mod ) {
	return get_theme_mod(
		$mod,
		eform_live_get_theme_mod_default( $mod )
	);
}

/**
 * Get theme mods by array.
 *
 * @param array $mods Associative array of key and defaults.
 * @return array Theme mod.
 */
function eform_live_get_theme_mods( $mods ) {
	$theme_mods = [];
	foreach ( $mods as $key ) {
		$theme_mods[ $key ] = eform_live_get_theme_mod( $key );
	}
	return $theme_mods;
}


/**
 * Get default for a registered and known eform theme mod.
 *
 * @param string $mod The theme mod key.
 * @return mixed The default value.
 */
function eform_live_get_theme_mod_default( $mod ) {
	$defaults = [
		// Navigation
		'nav_cta_label' => __( 'Try it now', 'eform-live' ),
		'nav_cta_url' => '#',
		// Social Stuff
		'social_facebook' => 'https://www.facebook.com/WPQuark/',
		'social_twitter' => 'https://twitter.com/WPQuark',
		'social_youtube' => 'https://www.youtube.com/c/WPQuark',
		'social_envato' => 'https://codecanyon.net/user/wpquark',
		// Frontpage hero section
		'hero_slogan' => __( 'WordPress Form Solution.', 'eform-live' ),
		'hero_subtitle' => __( 'Everything from payment, estimation, quizzes, surveys to data collection of all kinds. Trusted by over 10,000 website owners.', 'eform-live' ),
		'primary_cta_text' => __( 'Examples', 'eform-live' ),
		'primary_cta_url' => '#',
		'secondary_cta_text' => __( 'Buy', 'eform-live' ),
		'secondary_cta_url' => '#',
		'hero_video_cta_text' => __( 'Watch Video', 'eform-live' ),
		'hero_video_cta_id' => '#',
		// Brands
		'brands' => [],
		'brand_cta_link' => 'https://wpquark.com/kb/fsqm/fsqm-integration/',
		'brand_cta_label' => __( 'Our Integrations', 'eform-live' ),
		// Frontpage steps
		'fp_steps_title' => __( 'Build your forms in **seconds**_._', 'eform-live' ),
		'step_one_title' => __( 'Create', 'eform-live' ),
		'step_one_image' => '',
		'step_two_title' => __( 'Publish', 'eform-live' ),
		'step_two_points' => [
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Shortcode', 'eform-live' ),
			],
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Gutenberg', 'eform-live' ),
			],
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Standalone', 'eform-live' ),
			],
		],
		'step_three_title' => __( 'Collect', 'eform-live' ),
		'step_three_points' => [
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Payment', 'eform-live' ),
			],
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Data', 'eform-live' ),
			],
			[
				'step_svg' => '',
				'step_url' => '#',
				'step_label' => __( 'Reports', 'eform-live' ),
			],
		],
		'step_cta_label' => __( 'Buy', 'eform-live' ),
		'step_cta_link' => '#',
		// Frontpage testimonial
		'fp_testi_title' => __( 'From our great **Clients**_._', 'eform-live' ),
		'testimonials' => [],
		// Frontpage video demo
		'video_title' => __( 'The world\'s most **powerful form building tool**_._', 'eform-live' ),
		'video_media' => '',
		'video_poster' => '',
		'video_subtitle' => __( 'More than **_9500_ happy clients**_._', 'eform-live' ),
		'video_cta_text' => __( 'Get eForm', 'eform-live' ),
		'video_cta_link' => '#',
		// Frontend cards
		'card_max_items' => 2,
		'card_cta_label' => __( 'More Features', 'eform-live' ),
		// Single page Attraction
		'sp_bullhorn_title' => __( 'Try eForm Today', 'eform-live' ),
		'sp_bullhorn_desc' => __( 'The true all in one form solution for WordPress has arrived. Get started with eForm now.', 'eform-live' ),
		'sp_bullhorn_cta_one_label' => __( 'Try it for free', 'eform-live' ),
		'sp_bullhorn_cta_one_url' => '#',
		'sp_bullhorn_cta_two_label' => __( 'See examples', 'eform-live' ),
		'sp_bullhorn_cta_two_url' => '#',
	];
	if ( isset( $defaults[ $mod ] ) ) {
		return $defaults[ $mod ];
	}
	return '';
}

/**
 * Just a super-fast implementation of just bold
 * and italics markdowns.
 *
 * @param string $string Input string.
 * @return string HTML converted value.
 */
function eform_live_format_psuedo_md( $string ) {
	// Replace with bold
	$string = preg_replace_callback( '/\*\*(.*)\*\*/mU', function( $matches ) {
		return '<strong>' . $matches[1] . '</strong>';
	}, $string );
	// Replace with italics
	$string = preg_replace_callback( '/\*(.*)\*/mU', function( $matches ) {
		return '<em>' . $matches[1] . '</em>';
	}, $string );
	// Replace with accent
	$string = preg_replace_callback( '/\_(.*)\_/mU', function( $matches ) {
		return '<span class="efl-accent">' . $matches[1] . '</span>';
	}, $string );

	return $string;
}

/**
 * Get SVG from file system. Everything provided by this theme.
 *
 * @param string $svg_path Relative path of svg from /images/.
 * @return string SVG read from filesystem.
 */
function eform_live_get_svg( $svg_path ) {
	static $cached_svg = [];
	if ( ! isset( $cached_svg[ $svg_path ] ) ) {
		$cached_svg[ $svg_path ] = file_get_contents(
			get_template_directory() . '/images/' . $svg_path
		);
	}
	return $cached_svg[ $svg_path ];
}

/**
 * Get an SVG wrapped in a markup for easy styling.
 *
 * If you pass svg file name like 'checkmark.svg' then it can
 * search the template directory to load.
 *
 * @param string $svg The SVG markup.
 * @return string.
 */
function eform_live_get_svg_icon( $svg ) {
	// If svg is URL, then read it
	if ( stripos( $svg, '</svg>' ) === false ) {
		$svg = eform_live_get_svg( $svg );
	}
	return '<span class="efl-svg-icon">' . $svg . '</span>';
}


/**
 * Get available social links (SVGs) supported by
 * eForm Live Theme.
 *
 * @return array Associative array where key is the SVG name
 *               and value is the translated Social Platform name.
 */
function eform_live_get_available_socials() {
	$socials = [
		'facebook' => __( 'Facebook', 'eform-live' ),
		'twitter' => __( 'Twitter', 'eform-live' ),
		'youtube' => __( 'YouTube', 'eform-live' ),
		'envato' => __( 'Envato', 'eform-live' ),
	];
	return $socials;
}
