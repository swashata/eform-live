<?php
/**
 * EForm Live Preview functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eForm_Live_Preview
 */

// Composer
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Init the basic theme supports and functions
 */
require get_template_directory() . '/inc/class-eform-live-react-apps.php';
require get_template_directory() . '/inc/class-eform-live-init.php';
require get_template_directory() . '/inc/class-eform-live-cards.php';
$eform_live_init = EForm_Live_Init::instance();

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template functions for this theme.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Add kirki stuff
 */
require get_template_directory() . '/inc/class-kirki-installer-section.php';
require get_template_directory() . '/inc/class-eform-live-kirki.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/class-eform-live-customizer.php';
new EForm_Live_Customizer();
