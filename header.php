<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eForm_Live_Preview
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" id="eform-live-viewport" content="width=device-width, initial-scale=1">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#eceff1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="masthead" class="efl-site-header" role="banner">
		<div class="efl-site-header__container">
			<?php eform_live_site_logo(); ?>
			<nav id="site-navigation" class="efl-site-header__main-nav" role="navigation">
				<div class="efl-site-header__navoverflow"></div>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id' => 'primary-menu',
						'depth' => 6,
					)
				);
				?>
			</nav><!-- #site-navigation -->
			<?php if ( '' != eform_live_get_theme_mod( 'nav_cta_label' ) ) : ?>
				<a
					target="_blank"
					href="<?php echo esc_attr( eform_live_get_theme_mod( 'nav_cta_url' ) ); ?>"
					id="eform-live-cta-anchor"
					class="efl-site-header__cta efl-cta-btn"
				>
						<?php echo eform_live_get_theme_mod( 'nav_cta_label' ); ?>
				</a>
			<?php endif; ?>
			<button class="hamburger hamburger--3dxy efl-site-header__hamburger" type="button">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</div>
	</header><!-- #masthead -->
