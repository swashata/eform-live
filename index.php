<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eForm_Live_Preview
 */

// Header
get_header();

// Page Heading with breadcrumb
eform_live_page_heading(
	eform_live_get_page_title()
);

// Main wrapper
echo '<main class="efl-content" role="main" id="main">';

$has_post = false;

if ( have_posts() ) {
	// Archive page cards
	$has_post = true;
	echo '<div class="efl-content-cards">';
	/* Start the Loop */
	while ( have_posts() ) {
		the_post();
		/**
		 * Include the Post-Format-specific template for the content.
		 * If you want to override this in a child theme, then include a file
		 * called card-___.php (where ___ is the Post Format name) and that will be used instead.
		 */
		get_template_part( 'template-parts/card', get_post_format() );
	}
	echo '</div>'; // .efl-content-cards
} else {
	get_template_part( 'template-parts/content', 'none' );
}

echo '</main>'; // #main

if ( $has_post ) {
	eform_live_content_nav( 'nav-below' );
}

eform_live_bullhorn();

// Footer
get_footer();
